package com.tadmar.mvx.b2cshopec;

import io.quarkus.test.junit.QuarkusTest;

import java.util.logging.Logger;

import javax.inject.Inject;


import com.tadmar.mvx.b2cshopec.data.Repo;
import com.tadmar.mvx.b2cshopec.rest.ApiIntV1;
import com.tadmar.mvx.b2cshopec.services.v1.CustomerServMvx;

@QuarkusTest
public class RepoTests {

	public static final Logger LOG = Logger.getLogger(ApiIntV1.class.getName());
	
	@Inject Repo repo;	
	@Inject CustomerServMvx customerServMvx;

/*
	@Test
	public void testkWhloForOrderLine0() {
		
		var itno = "XX";
		var whlo = repo.findWhloForOrderLine(200, itno);
		whlo = whlo.trim().length() == 0 ? "11N" : whlo;
		System.out.println(itno + " " + whlo);
		Assertions.assertTrue(whlo.equals("11N"));
	}
	
	@Test
	public void testkWhloForOrderLine1() {
		
		var itno = "GRUN-250129";
		var whlo = repo.findWhloForOrderLine(200, itno);
		whlo = whlo.trim().length() == 0 ? "11N" : whlo;
		System.out.println(itno + " " + whlo);
		Assertions.assertTrue(whlo.equals("11O"));
	}

	@Test
	public void testkWhloForOrderLine2() {
		
		var itno = "NNNN-950783";
		var whlo = repo.findWhloForOrderLine(200, itno);
		whlo = whlo.trim().length() == 0 ? "11N" : whlo;
		System.out.println(itno + " " + whlo);
		Assertions.assertTrue(whlo.equals("11N"));
	}

	@Test
	public void testFindOcusmaByTxcd() {
	
		var orunid = 3177; //5265;
		var cunoExp = "T520192487"; //"T520196038";
		
		var cuno = "";
		var adid = "";
		
		List<CunoAdid> cunoAdids = new ArrayList<>();
		repo.getOrdersRegistered().stream().filter(of -> of.getOrunid() == orunid).forEach(o -> {
			var cunoAdid = customerServMvx.findOrCreateCustomerMvx(o);
			cunoAdids.add(cunoAdid);
		});		
		cuno = cunoAdids.get(0).getCuno();
		adid = cunoAdids.get(0).getAdid();
		
		Assertions.assertTrue(cuno.equals(cunoExp));
	}
	

	
	@Test
	public void testCreateOcusm() {
	
		var orunid = 5383;
		
		var orflows = repo.getOrdersRegistered();
		Orflow orflow = null;
		for ( var or : orflows) {
			if ( or.getOrunid().intValue() == orunid) {
				orflow = or;
			}
		}
		customerServMvx.findOrCreateCustomerMvx(orflow);
	}
*/
}

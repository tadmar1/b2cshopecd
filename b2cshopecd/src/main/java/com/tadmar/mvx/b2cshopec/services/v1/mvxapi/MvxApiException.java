package com.tadmar.mvx.b2cshopec.services.v1.mvxapi;

public class MvxApiException extends Exception {

	private static final long serialVersionUID = 1L;
	
	public MvxApiException() {}
	public MvxApiException(String message) {
		super(message);
	}
}

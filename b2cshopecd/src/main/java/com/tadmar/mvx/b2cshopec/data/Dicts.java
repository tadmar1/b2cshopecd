package com.tadmar.mvx.b2cshopec.data;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;

import com.tadmar.mvx.b2cshopec.model.mvx.EmIntStr;

import io.quarkus.runtime.Startup;

@Startup
@ApplicationScoped
public class Dicts {
	
	private List<EmIntStr> shippings;
	private List<EmIntStr> payments;

	@PostConstruct
	void init() {
		loadDicts();
	}

	public String getPaymentNameById(int id) {
		Optional<EmIntStr> payment = payments.stream().filter(p -> p.getParVal1().intValue() == id).findFirst();
		return payment.isPresent() ? payment.get().getParVal2() : "";
	}

	public String getShippingNameById(int id) {
		Optional<EmIntStr> shipping = shippings.stream().filter(p -> p.getParVal1().intValue() == id).findFirst();
		return shipping.isPresent() ? shipping.get().getParVal2() : "";
	}

	private void loadDicts() {
		
		shippings = new ArrayList<>();
		shippings.add(EmIntStr.builder().parVal1(1).parVal2("TADMAR").build());
		shippings.add(EmIntStr.builder().parVal1(2).parVal2("Poczta Polska").build());
		shippings.add(EmIntStr.builder().parVal1(3).parVal2("Poczta Polska (priorytet)").build());
		shippings.add(EmIntStr.builder().parVal1(5).parVal2("Kurier DHL Express").build());
		shippings.add(EmIntStr.builder().parVal1(6).parVal2("Kurier GLS Poland").build());
		shippings.add(EmIntStr.builder().parVal1(7).parVal2("Kurier DPD").build());
		shippings.add(EmIntStr.builder().parVal1(9).parVal2("Allegro").build());
		shippings.add(EmIntStr.builder().parVal1(10).parVal2("Poczta Polska").build());
		shippings.add(EmIntStr.builder().parVal1(12).parVal2("Kurier InPost").build());
		shippings.add(EmIntStr.builder().parVal1(13).parVal2("Paczkomaty InPost").build());
		shippings.add(EmIntStr.builder().parVal1(14).parVal2("Kurier DHL").build());
		shippings.add(EmIntStr.builder().parVal1(16).parVal2("Kurier DHL").build());
		shippings.add(EmIntStr.builder().parVal1(19).parVal2("Kurier InPost").build());
		shippings.add(EmIntStr.builder().parVal1(21).parVal2("Paczkomaty Paczka w Weekend").build());
		shippings.add(EmIntStr.builder().parVal1(24).parVal2("Paczkomaty").build());
		shippings.add(EmIntStr.builder().parVal1(25).parVal2("Kurier DHL Parcel MAX").build());
		shippings.add(EmIntStr.builder().parVal1(27).parVal2("Pobranie DHL").build());
		shippings.add(EmIntStr.builder().parVal1(17).parVal2("Punkty DHL").build());
		shippings.add(EmIntStr.builder().parVal1(32).parVal2("Transport własny ok Poznania").build());
		
		payments = new ArrayList<>();
		payments.add(EmIntStr.builder().parVal1(1).parVal2("Płatność przy odbiorze").build());
		payments.add(EmIntStr.builder().parVal1(2).parVal2("Pobranie").build());
		payments.add(EmIntStr.builder().parVal1(3).parVal2("Przelew natychmiastowy").build());
		payments.add(EmIntStr.builder().parVal1(4).parVal2("mBank mRaty").build());
		payments.add(EmIntStr.builder().parVal1(5).parVal2("PayU").build());
		payments.add(EmIntStr.builder().parVal1(6).parVal2("DotPay").build());
		payments.add(EmIntStr.builder().parVal1(7).parVal2("Przelewy24.pl").build());
		payments.add(EmIntStr.builder().parVal1(8).parVal2("mTransfer").build());
		payments.add(EmIntStr.builder().parVal1(9).parVal2("multiTransfer").build());
		payments.add(EmIntStr.builder().parVal1(10).parVal2("eRaty Santander Consumer Bank S.A.").build());
		payments.add(EmIntStr.builder().parVal1(11).parVal2("PayPal").build());
		payments.add(EmIntStr.builder().parVal1(13).parVal2("Allegro").build());
		payments.add(EmIntStr.builder().parVal1(14).parVal2("Darmowe zamówienie").build());
		payments.add(EmIntStr.builder().parVal1(15).parVal2("PayPal Express Checkout").build());
		payments.add(EmIntStr.builder().parVal1(16).parVal2("Klarna").build());
		payments.add(EmIntStr.builder().parVal1(17).parVal2("Braintree").build());
		payments.add(EmIntStr.builder().parVal1(19).parVal2("Przelew tradycyjny").build());
		payments.add(EmIntStr.builder().parVal1(20).parVal2("Pobranie DHL").build());
	}
	
	
}

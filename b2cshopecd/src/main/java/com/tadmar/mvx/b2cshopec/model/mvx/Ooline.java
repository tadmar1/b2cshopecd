package com.tadmar.mvx.b2cshopec.model.mvx;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Getter @Setter @EqualsAndHashCode @ToString @NoArgsConstructor @AllArgsConstructor @Builder
public class Ooline implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	private String obunid;
	private Integer obcono;
	private String oborno;
	private Integer obponr;
	private Integer obposx;
	private String obitno;
	private BigDecimal obsapr;
	private BigDecimal obnepr;
	private String obprrf;
	private String obprmo;
	private Integer obdic1;
	private Integer obdic2;
	private Integer obdic3;
	private Integer obdic4;
	private Integer obdic5;
	private Integer obdic6;
	private BigDecimal obdip1;
	private BigDecimal obdip2;
	private BigDecimal obdip3;
	private BigDecimal obdip4;
	private BigDecimal obdia5;
	private BigDecimal obdip6;
}

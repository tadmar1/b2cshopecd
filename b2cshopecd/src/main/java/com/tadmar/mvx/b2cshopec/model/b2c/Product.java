package com.tadmar.mvx.b2cshopec.model.b2c;

import java.io.Serializable;

import javax.json.bind.annotation.JsonbProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@EqualsAndHashCode @ToString @NoArgsConstructor @AllArgsConstructor @Builder
public class Product implements Serializable {

	private static final long serialVersionUID = 1L;

	
	@JsonbProperty("product_id")
	private Integer productId;
	
	@JsonbProperty("stock_id")
	private Integer stockId;

	@JsonbProperty("add_date")
	private String dtAd;

	@JsonbProperty("edit_date")
	private String dtEd;
	
	@JsonbProperty("gauge_id")
	private Integer gaugId;

	@JsonbProperty("stock")
	private Stock stock;
	
	@JsonbProperty("translations")
	private Translations translations;
	
	@JsonbProperty("attributes")
	private Object attributes;
	
	public Integer getProductId() {
		return productId;
	}

	public void setProductId(Integer productId) {
		this.productId = productId;
	}

	public Integer getStockId() {
		return stockId;
	}

	public void setStockId(Integer stockId) {
		this.stockId = stockId;
	}

	public Stock getStock() {
		return stock;
	}

	public void setStock(Stock stock) {
		this.stock = stock;
	}

	public Translations getTranslations() {
		return translations;
	}

	public void setTranslations(Translations translations) {
		this.translations = translations;
	}

	public Object getAttributes() {
		return attributes;
	}

	public void setAttributes(Object attributes) {
		this.attributes = attributes;
	}

	public String getDtAd() {
		return dtAd;
	}

	public void setDtAd(String dtAd) {
		this.dtAd = dtAd;
	}

	public String getDtEd() {
		return dtEd;
	}

	public void setDtEd(String dtEd) {
		this.dtEd = dtEd;
	}

	public Integer getGaugId() {
		return gaugId;
	}

	public void setGaugId(Integer gaugId) {
		this.gaugId = gaugId;
	}

}

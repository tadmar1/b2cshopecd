package com.tadmar.mvx.b2cshopec.services.v1;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.core.Response;

import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.json.JSONArray;
import org.json.JSONException;

import com.tadmar.mvx.b2cshopec.boot.B2cApiConfig;
import com.tadmar.mvx.b2cshopec.model.cdpserv.DocDlnu;
import com.tadmar.mvx.b2cshopec.model.mvx.EmStr2;
import com.tadmar.mvx.b2cshopec.model.mvx.EmStr5;
import com.tadmar.mvx.b2cshopec.rest.ApiIntV1;
import com.tadmar.mvx.b2cshopec.rest.client.ApiExtV1ClientCdp;

@ApplicationScoped
public class CdpServ {

	public static final Logger LOG = Logger.getLogger(ApiIntV1.class.getName());

	@Inject @RestClient ApiExtV1ClientCdp apiExtV1ClientCdp;
	@Inject B2cApiConfig b2cApiConfig;
	
	@PostConstruct
	void init() {}

	public List<DocDlnu> getDlnu(String user, String dtFr, String dtTo) {
		
		var query = "{ \"columns\": [\"Id\", \"ExternalId\", \"ShippingToNumber\", \"Number\", \"DocumentLK\"], \"dateTypeId\": 1, \"dateFrom\": \"@dtFr\", \"dateTo\": \"@dtTo\" }";
		query = query.replace("@dtFr", dtFr).replace("@dtTo", dtTo);
		var secToken = b2cApiConfig.requestTokenCdp(user);
		LOG.info(query);
		LOG.info(secToken);
		List<DocDlnu> docDlnus = new ArrayList<>();;
		try {
			Response resp = apiExtV1ClientCdp.getDocDlnos(secToken, query);
			var respRaw = resp.readEntity(String.class);

			var respList = convertGetDocumentsLinesObj(jsonStringToListOfObjects(respRaw));
			respList.stream().filter(o -> o.getParVal1() != null && o.getParVal2() != null).forEach(fo -> {
				var extIdArr = fo.getParVal2().split("-");
				if (extIdArr.length >= 6) {
					var docDlno =DocDlnu.builder().externalId(extIdArr[5]).shippingNumber(fo.getParVal3().trim()).id(fo.getParVal1().trim())
					.number(fo.getParVal4().trim()).documentLk(fo.getParVal5().trim()).build();
					docDlnus.add(docDlno);
				}
			});
			resp.close();
		} catch(Exception ex) {
			LOG.info("CDP-docDlno: " + ex.getMessage());
		}
		LOG.info("docDlnus: " + docDlnus.size());
		return docDlnus;
	}
	
	public List<EmStr2> getDlnuSh(String user, String dtFr, String dtTo) {
		
		var query = "{ \"columns\": [ \"AssignedDespatchDocument.DocumentExternalId\", \"ShippingToNumber\" ], \"shippingCarrierNumber\": null, \"shippingCarrierTypeIds\": null, \"createdDateFrom\": \"@dtFr\", \"createdDateTo\": null, \"DocumentTypeIds\": [7] } ";
		query = query.replace("@dtFr", dtFr).replace("@dtTo", dtTo);
		var secToken = b2cApiConfig.requestTokenCdp(user);
		List<EmStr2> docDlnus = new ArrayList<>();
		try {
			Response resp = apiExtV1ClientCdp.getDocDlnosSh(secToken, query);
			var respList = convertGetDocumentsLinesObjSh(jsonStringToListOfObjects(resp.readEntity(String.class)));
			respList.stream().filter(o -> o.getParVal1() != null && o.getParVal2() != null).forEach(fo -> {
					var extIdArr = fo.getParVal1().split("-");
					if (extIdArr.length >= 6) {
						fo.setParVal1(extIdArr[5]);
						docDlnus.add(fo);
					}
			});
			resp.close();
		} catch(Exception ex) {
			LOG.info("SHP-docDlno: " + ex.getMessage());
		}
		
		return docDlnus;
	}

	public static List<Object> jsonStringToListOfObjects(String jsonString) throws JSONException {

		List<Object> objects = (new JSONArray(jsonString)).toList();
	    if ( !objects.isEmpty()) { objects.remove(0); }
	    return objects;
	}

	private List<EmStr5> convertGetDocumentsLinesObj(List<Object> objects) {
		
		List<EmStr5> outs = new ArrayList<>();
		
		for (var obj : objects) {
			var builder = EmStr5.builder();
			var i = 0;
			@SuppressWarnings("unchecked")
			var elem = (List<Object>) obj ;
			for (var e : elem) {
				switch (i) {
				case 0 :
					builder.parVal1(String.valueOf( (Integer) e ));
					break;
				case 1:
					builder.parVal2( (String) e == null ? "" : (String) e);
					break;
				case 2:
					builder.parVal3( (String) e == null ? "" : (String) e);
					break;
				case 3:
					builder.parVal4( (String) e == null ? "" : (String) e );
					break;
				case 4:
					builder.parVal5( (String) e == null ? "" : (String) e );
					break;
				default :	
				}
				i++;
			}
			outs.add(builder.build());
		}
		return outs;
	}
	private List<EmStr2> convertGetDocumentsLinesObjSh(List<Object> objects) {
		
		List<EmStr2> outs = new ArrayList<>();
		
		for (var obj : objects) {
			var builder = EmStr2.builder();
			var i = 0;
			@SuppressWarnings("unchecked")
			var elem = (List<Object>) obj ;
			for (var e : elem) {
				switch (i) {
				case 0:
					builder.parVal1( (String) e == null ? "" : (String) e);
					break;
				case 1:
					builder.parVal2( (String) e == null ? "" : (String) e);
					break;
				default :	
				}
				i++;
			}
			outs.add(builder.build());
		}
		return outs;
	}

}

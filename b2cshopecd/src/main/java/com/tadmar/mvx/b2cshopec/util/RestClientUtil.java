package com.tadmar.mvx.b2cshopec.util;

import java.net.MalformedURLException;

import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedHashMap;
import javax.ws.rs.core.MultivaluedMap;

public class RestClientUtil {
	
	public static  MultivaluedMap<String, Object> buildRestClientStdHeader(String accType, String accToken) throws MalformedURLException { 
		
		var headers = new MultivaluedHashMap<String, Object>();
		headers.add(HttpHeaders.AUTHORIZATION, accType.trim() + " " + accToken.trim());
		headers.add("cache-control", "no-cache");
		headers.add("expires", "-1");
		headers.add(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON);
		headers.add(HttpHeaders.CONTENT_TYPE , MediaType.APPLICATION_JSON);  
		return headers;
	}	
	
	public static void delay(int timeMiliSec) {
		try {Thread.sleep(timeMiliSec);} catch (InterruptedException e) {}
	}


}

package com.tadmar.mvx.b2cshopec.rest.client;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;

@RegisterRestClient(configKey="mf-api")
@Path("/")
@Produces("application/json")
@Consumes("application/json")
public interface ApiExtV1ClientMf {
	
	@Path("search/nip/{nip}")
	@GET
	Response searchCustomerByNip( 
		@PathParam("nip")String nip,	
		@QueryParam("date") String date
	);
}

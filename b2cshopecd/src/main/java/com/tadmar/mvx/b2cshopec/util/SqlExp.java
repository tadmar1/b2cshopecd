package com.tadmar.mvx.b2cshopec.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.nio.charset.StandardCharsets;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.FactoryConfigurationError;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

public class SqlExp {

	private static String SQL_EXPRESSIONS_BASE_NAME = "SqlExpressions";
	private static String SQL_EXPRESSIONS_BASE_NAME_SUFFIX = ".xml";
	private static final String LEFT_INDENT_TO_OMMIT = "\t\t\t\t\t\t\t\t\t\t";

	private Document doc;
	
	public SqlExp(String value) {
		doc = readSqlQueryFile(SQL_EXPRESSIONS_BASE_NAME + value + SQL_EXPRESSIONS_BASE_NAME_SUFFIX);
	}
	
	public String getSqlExpressionEntry(String searchKey) {
		return getSqlExpression(doc, searchKey);
	}

	private String getSqlExpression(Document docExp, String searchKey) {
		try {
			XPathExpression expr = XPathFactory.newInstance().newXPath().compile("/root/data[@name=\"" + 
					searchKey + "\"]/value");
			return ((String) expr.evaluate(docExp, XPathConstants.STRING)).replace("\n" + LEFT_INDENT_TO_OMMIT , "\n");
		} catch (Exception parserException ){
			return "";
		}
	}

	private Document readSqlQueryFile(String fileName) {
	    try {
		    InputSource is = new InputSource();
		    is.setCharacterStream(new StringReader(getFile(fileName)));
		    return DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(is);
		} catch (SAXException | IOException | ParserConfigurationException | FactoryConfigurationError e) {
			e.printStackTrace();
		}
	    return null;
	}

	private String getFile(String fileName) {
		 
		ClassLoader classLoader = getClass().getClassLoader();
		BufferedReader in = new BufferedReader(new InputStreamReader(classLoader.getResourceAsStream(fileName), StandardCharsets.UTF_8));
		StringBuffer rb = new StringBuffer();
		in.lines().forEach(l -> {rb.append(l).append("\n");});
		return rb.toString();
	}
}

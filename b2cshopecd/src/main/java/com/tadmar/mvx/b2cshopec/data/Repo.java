package com.tadmar.mvx.b2cshopec.data;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.control.ActivateRequestContext;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import static javax.transaction.Transactional.TxType.*;

import com.tadmar.mvx.b2cshopec.model.mvx.EmInt;
import com.tadmar.mvx.b2cshopec.model.mvx.EmIntStr;
import com.tadmar.mvx.b2cshopec.model.mvx.EmStr;
import com.tadmar.mvx.b2cshopec.model.mvx.EmStr2Bd2;
import com.tadmar.mvx.b2cshopec.model.mvx.Mhdisl;
import com.tadmar.mvx.b2cshopec.model.mvx.Ocuscr3;
import com.tadmar.mvx.b2cshopec.model.mvx.Ocusma;
import com.tadmar.mvx.b2cshopec.model.mvx.Ooline;
import com.tadmar.mvx.b2cshopec.model.mvx.Orflow;
import com.tadmar.mvx.b2cshopec.model.mvx.ProductPriceErp;
import com.tadmar.mvx.b2cshopec.model.mvx.ProductPriceNew;
import com.tadmar.mvx.b2cshopec.model.mvx.ProductStockPriceErp;
import com.tadmar.mvx.b2cshopec.model.mvx.ShbalSpOffers;
import com.tadmar.mvx.b2cshopec.util.DateFormatter;
import com.tadmar.mvx.b2cshopec.util.DateUtils;
import com.tadmar.mvx.b2cshopec.util.SqlExp;
import com.tadmar.mvx.b2cshopec.util.SqlExpAnotations.SqlExpKey;

@ApplicationScoped
public class Repo {

	private static final Logger LOG = Logger.getLogger(Repo.class.getName());

	@Inject 
    EntityManager emMvx;

	@Inject @SqlExpKey(key="")
	SqlExp sqlExpressionService;
	
	@PostConstruct
	void init() {
	}
	
	@Transactional(NOT_SUPPORTED)
	@ActivateRequestContext
	public List<ProductStockPriceErp> getProductsStockErp(int cono) {

		String sqlT = sqlExpressionService.getSqlExpressionEntry("Repo.getProductsStockErp").
			replace("{cono}", String.valueOf(cono) )
   			;
		try {
			@SuppressWarnings("unchecked")
			List<ProductStockPriceErp> l = emMvx.createNativeQuery(sqlT, ProductStockPriceErp.class).getResultList();
			return l;
		} catch (Exception ex) {
			LOG.info(ex.getMessage());
			return Collections.emptyList();
		}
	}

	@Transactional(NOT_SUPPORTED)
	@ActivateRequestContext
	public List<ShbalSpOffers> getProductsShbalSpOffers(int cono) {

		String sqlT = sqlExpressionService.getSqlExpressionEntry("Repo.getProductsShbalSpOffers").
			replace("{cono}", String.valueOf(cono) )
   			;
		try {
			@SuppressWarnings("unchecked")
			List<ShbalSpOffers> l = emMvx.createNativeQuery(sqlT, ShbalSpOffers.class).getResultList();
			return l;
		} catch (Exception ex) {
			LOG.info(ex.getMessage());
			return Collections.emptyList();
		}
	}

	@Transactional(NOT_SUPPORTED)
	@ActivateRequestContext
	public List<ProductStockPriceErp> getProductsPriceErp(int cono) {

		String sqlT = sqlExpressionService.getSqlExpressionEntry("Repo.getProductsPriceErp").
			replace("{cono}", String.valueOf(cono) )
   			;
		try {
			@SuppressWarnings("unchecked")
			List<ProductStockPriceErp> l = emMvx.createNativeQuery(sqlT, ProductStockPriceErp.class).getResultList();
			return l;
		} catch (Exception ex) {
			LOG.info(ex.getMessage());
			return Collections.emptyList();
		}
	}

	@Transactional(NOT_SUPPORTED)
	@ActivateRequestContext
	public List<ProductPriceErp> getProductsPiceErpForStatus(int cono,int status) {
		
		String sqlT = sqlExpressionService.getSqlExpressionEntry("Repo.getProductsPiceErpForStatus")
				.replace("{cono}", String.valueOf(cono))
				.replace("{0}", String.valueOf(status)) 
	   			;
		try {
			@SuppressWarnings("unchecked")
			List<ProductPriceErp> l = emMvx.createNativeQuery(sqlT, ProductPriceErp.class).getResultList();
			return l;
		} catch (Exception ex) {
			LOG.info(ex.getMessage());
			return null;
		}
	}
	

	@Transactional(NOT_SUPPORTED)
	@ActivateRequestContext
	public List<Orflow> getOrdersRegistered() {
		
		String sqlT = sqlExpressionService.getSqlExpressionEntry("Repo.getOrdersRegistered")
	   			;
		try {
			@SuppressWarnings("unchecked")
			List<Orflow> l = emMvx.createNativeQuery(sqlT, Orflow.class).getResultList();
			return l;
		} catch (Exception ex) {
			LOG.info(ex.getMessage());
			return null;
		}
	}

	@Transactional(NOT_SUPPORTED)
	@ActivateRequestContext
	public List<Orflow> getOrdersWithDlnoEmpty() {
		
		String sqlT = sqlExpressionService.getSqlExpressionEntry("Repo.getOrdersWithDlnoEmpty")
	   			;
		try {
			@SuppressWarnings("unchecked")
			List<Orflow> l = emMvx.createNativeQuery(sqlT, Orflow.class).getResultList();
			return l;
		} catch (Exception ex) {
			LOG.info(ex.getMessage());
			return null;
		}
	}

	@Transactional(NOT_SUPPORTED)
	@ActivateRequestContext
	public List<Orflow> getOrdersLastWithDlno() {
		
		String sqlT = sqlExpressionService.getSqlExpressionEntry("Repo.getOrdersLastWithDlno")
	   			;
		try {
			@SuppressWarnings("unchecked")
			List<Orflow> l = emMvx.createNativeQuery(sqlT, Orflow.class).getResultList();
			return l;
		} catch (Exception ex) {
			LOG.info(ex.getMessage());
			return null;
		}
	}
	
	@ActivateRequestContext
	public List<EmStr> getOrdersInvNotSend() {
		
		String sqlT = sqlExpressionService.getSqlExpressionEntry("Repo.getOrdersInvNotSend")
	   			;
		try {
			@SuppressWarnings("unchecked")
			List<EmStr> l = emMvx.createNativeQuery(sqlT, EmStr.class).getResultList();
			return l;
		} catch (Exception ex) {
			LOG.info(ex.getMessage());
			return null;
		}
	}
	
	
	@Transactional(NOT_SUPPORTED)
	@ActivateRequestContext
	public List<Orflow> getOrdersForStatuses(int cono, int statusFrom, int statusTo) {
		
		String sqlT = sqlExpressionService.getSqlExpressionEntry("Repo.getOrdersForStatuses")
				.replace("{cono}", String.valueOf(cono))
				.replace("{0}", String.valueOf(statusFrom)) 
				.replace("{1}", String.valueOf(statusTo)) 
	   			;
		try {
			@SuppressWarnings("unchecked")
			List<Orflow> l = emMvx.createNativeQuery(sqlT, Orflow.class).getResultList();
			return l;
		} catch (Exception ex) {
			LOG.info(ex.getMessage());
			return null;
		}
	}

	@Transactional(NOT_SUPPORTED)
	@ActivateRequestContext
	public String findCunoForTxCd(int cono, String txcd) {

		txcd = txcd.replace("-", "");
		String sqlT = sqlExpressionService.getSqlExpressionEntry("Repo.findCunoForTxCd")
			.replace("{cono}", String.valueOf(cono))
			.replace("{0}", txcd)
	   		;
		try {
			@SuppressWarnings("unchecked")
			List<EmStr> l = emMvx.createNativeQuery(sqlT, EmStr.class).getResultList();
			return l.size() > 0 ? l.get(0).getParVal().trim() : "";
		} catch (Exception ex) {
			LOG.info(ex.getMessage());
			return "";
		}
	}
	
	@Transactional(NOT_SUPPORTED)
	@ActivateRequestContext
	public List<Ocusma> findOcusmaForTxCd(int cono, String txcd) {

		txcd = txcd.replace("-", "");
		String sqlT = sqlExpressionService.getSqlExpressionEntry("Repo.findOcusmaForTxCd")
			.replace("{cono}", String.valueOf(cono))
			.replace("{0}", txcd)
	   		;
		try {
			@SuppressWarnings("unchecked")
			List<Ocusma> l = emMvx.createNativeQuery(sqlT, Ocusma.class).getResultList();
			return l;
		} catch (Exception ex) {
			LOG.info(ex.getMessage());
			return Collections.emptyList();
		}
	}

	@Transactional(NOT_SUPPORTED)
	@ActivateRequestContext
	public List<Ocusma> findOcusmaForCuno(int cono, String cuno) {

		String sqlT = sqlExpressionService.getSqlExpressionEntry("Repo.findOcusmaForCuno")
			.replace("{cono}", String.valueOf(cono))
			.replace("{0}", cuno.trim())
	   		;
		try {
			@SuppressWarnings("unchecked")
			List<Ocusma> l = emMvx.createNativeQuery(sqlT, Ocusma.class).getResultList();
			return l;
		} catch (Exception ex) {
			LOG.info(ex.getMessage());
			return Collections.emptyList();
		}
	}

	@Transactional(NOT_SUPPORTED)
	@ActivateRequestContext
	public List<Orflow> findOrflowByMail(String email) {

		String sqlT = sqlExpressionService.getSqlExpressionEntry("Repo.findOrflowByMail")
			.replace("{0}", email.trim())
	   		;
		try {
			@SuppressWarnings("unchecked")
			List<Orflow> l = emMvx.createNativeQuery(sqlT, Orflow.class).getResultList();
			return l;
		} catch (Exception ex) {
			LOG.info(ex.getMessage());
			return Collections.emptyList();
		}
	}

	@Transactional(NOT_SUPPORTED)
	@ActivateRequestContext
	public String getCustMvxNextNumberT(int cono, String maxNumT) {

		String sqlT = sqlExpressionService.getSqlExpressionEntry("Repo.getCustMvxNextNumberT")
			.replace("{cono}", String.valueOf(cono))
			.replace("{0}", maxNumT.trim())
	   		;
		try {
			@SuppressWarnings("unchecked")
			List<EmStr> l = emMvx.createNativeQuery(sqlT, EmStr.class).getResultList();
			return l.get(0).getParVal();
		} catch (Exception ex) {
			LOG.info(ex.getMessage());
			return "";
		}
	}
	
	@Transactional(NOT_SUPPORTED)
	@ActivateRequestContext
	public String getCustMvxNextNumberAdid(int cono, String cuno) {

		String sqlT = sqlExpressionService.getSqlExpressionEntry("Repo.getCustMvxNextNumberAdid")
			.replace("{cono}", String.valueOf(cono))
			.replace("{0}", cuno.trim())
	   		;
		try {
			@SuppressWarnings("unchecked")
			List<EmStr> l = emMvx.createNativeQuery(sqlT, EmStr.class).getResultList();
			return !l.isEmpty() ? l.get(0).getParVal() : "";
		} catch (Exception ex) {
			LOG.info(ex.getMessage());
			return "";
		}
	}

	@Transactional(NOT_SUPPORTED)
	@ActivateRequestContext
	public String findCustMvxNumberAdid(int cono, String cuno, String cua2, String cua3, String pono) {

		String sqlT = sqlExpressionService.getSqlExpressionEntry("Repo.findCustMvxNumberAdid")
			.replace("{cono}", String.valueOf(cono))
			.replace("{0}", cuno.trim())
			.replace("{1}", cua2.trim())		
			.replace("{2}", pono.trim() + " " + cua3.trim())	   		;
		try {
			@SuppressWarnings("unchecked")
			List<EmStr> l = emMvx.createNativeQuery(sqlT, EmStr.class).getResultList();
			return !l.isEmpty() ? l.get(0).getParVal() : "";
		} catch (Exception ex) {
			LOG.info(ex.getMessage());
			return "";
		}
	}

	

	@Transactional(NOT_SUPPORTED)
	@ActivateRequestContext
	public String findAdidForCunoAndTexts(int cono, String cuno, String textName, String textCity) {

		textName = "%" + textName.trim().replace(" ", "%").replace("\"", "").replace("\'", "").toUpperCase() + "%";
		textCity = "%" + textCity.trim().replace(" ", "%").toUpperCase() + "%";
		
		String sqlT = sqlExpressionService.getSqlExpressionEntry("Repo.findAdidForCunoAndTexts")
			.replace("{cono}", String.valueOf(cono))
			.replace("{0}", cuno)
			.replace("{1}", textName)
			.replace("{2}", textCity)
	   		;
		try {
			@SuppressWarnings("unchecked")
			List<EmStr> l = emMvx.createNativeQuery(sqlT, EmStr.class).getResultList();
			return l.size() > 0 ? l.get(0).getParVal().trim() : "";
		} catch (Exception ex) {
			LOG.info(ex.getMessage());
			return "";
		}
	}

	@ActivateRequestContext
	public String findItemForCode(String code) {

		String sqlT = sqlExpressionService.getSqlExpressionEntry("Repo.findItemForCode")
			.replace("{0}", code)
	   		;
		try {
			@SuppressWarnings("unchecked")
			List<EmStr> l = emMvx.createNativeQuery(sqlT, EmStr.class).getResultList();
			return l.size() > 0 ? l.get(0).getParVal().trim() : "";
		} catch (Exception ex) {
			LOG.info(ex.getMessage());
			return "";
		}
	}

	@Transactional(REQUIRED)
	@ActivateRequestContext
	public int updateBatchOrderHeaderWhlo(String xorno, String whlo) {

		String sqlT = sqlExpressionService.getSqlExpressionEntry("Repo.updateBatchOrderHeaderWhlo")
			.replace("{0}", xorno.trim())
			.replace("{1}", whlo.trim())
		;
		try {
			return emMvx.createNativeQuery(sqlT).executeUpdate();
		} catch (Exception ex) {
			LOG.info(ex.getMessage());
			return 0;
		}
	}

	@Transactional(REQUIRED)
	@ActivateRequestContext
	public int updateBatchOrderHeaderOrtp(String orno, String whlo) {

		String sqlT = sqlExpressionService.getSqlExpressionEntry("Repo.updateBatchOrderHeaderOrtp")
			.replace("{0}", orno.trim())
			.replace("{1}", whlo.trim())
		;
		try {
			return emMvx.createNativeQuery(sqlT).executeUpdate();
		} catch (Exception ex) {
			LOG.info(ex.getMessage());
			return 0;
		}
	}
	
	@Transactional(REQUIRED)
	@ActivateRequestContext
	public void updateProductsPriceErpStatus(int cono, String productcd, int status) {

		String sqlT = sqlExpressionService.getSqlExpressionEntry("Repo.updateProductsPriceErpStatus")
				.replace("{cono}", String.valueOf(cono))
				.replace("{0}", productcd.trim())
				.replace("{1}", String.valueOf(status)) 
	   			;
		try {
			emMvx.createNativeQuery(sqlT).executeUpdate();
		} catch (Exception ex) {
			LOG.info(ex.getMessage());
		}
	}

	@Transactional(REQUIRED)
	@ActivateRequestContext
	public void updateProductsPriceErpStatusAll(int cono) {

		String sqlT = sqlExpressionService.getSqlExpressionEntry("Repo.updateProductsPriceErpStatusAll")
				.replace("{cono}", String.valueOf(cono))
	   			;
		try {
			emMvx.createNativeQuery(sqlT).executeUpdate();
		} catch (Exception ex) {
			LOG.info(ex.getMessage());
		}
	}

	@Transactional(NOT_SUPPORTED)
	@ActivateRequestContext
	public int getOprmtxForCunoCount(int cono, String cuno) {

		String sqlT = sqlExpressionService.getSqlExpressionEntry("Repo.getOprmtxForCunoCount")
				.replace("{0}", cuno.trim())
				;
		try {
			@SuppressWarnings("unchecked")
			List<EmInt> l = emMvx.createNativeQuery(sqlT, EmInt.class).getResultList();
			return l.get(0).getParVal().intValue();
		} catch (Exception ex) {
			LOG.info(ex.getMessage());
			return 0;
		}
	}

	@Transactional(REQUIRED)
	@ActivateRequestContext
	public int createOprmtxForCuno(int cono, String cuno) {

		String sqlT = sqlExpressionService.getSqlExpressionEntry("Repo.createOprmtxForCuno")
			.replace("{0}", String.valueOf(cono))
			.replace("{1}", cuno.trim())
			.replace("{2}", DateUtils.NowToDateMvx())
			.replace("{3}", DateUtils.NowToTimeMvx())
			.replace("{4}", DateUtils.NowToDateMvx())
			.replace("{5}", "XJOB")
		;
		try {
			return emMvx.createNativeQuery(sqlT).executeUpdate();
		} catch (Exception ex) {
			LOG.info(ex.getMessage());
			return 0;
		}
	}

	@SuppressWarnings("unchecked")
	@Transactional(NOT_SUPPORTED)
	@ActivateRequestContext
	public String getUnmsFromMitmas(int cono, String itno ) {
		
		List<EmStr> l = emMvx.createNativeQuery(sqlExpressionService.getSqlExpressionEntry("Repo.getUnmsFromMitmas")
				.replace("{cono}", String.valueOf(cono))
				.replace("{0}", itno.trim()),
				EmStr.class).getResultList();
		return l.size() > 0 ? l.get(0).getParVal().trim() : "SZT";
	}

	@Transactional(REQUIRED)
	@ActivateRequestContext
	public int createOprbasForCunoItno(int cono, String cuno, String itno, BigDecimal priceReq, 
			String unms, int dayOffset, String orno) {

		String sqlT = sqlExpressionService.getSqlExpressionEntry("Repo.createOprbasForCunoItno")
			.replace("{0}", String.valueOf(cono))
			.replace("{2}", "PLN")
			.replace("{1}", cuno.trim())
			.replace("{3}", DateUtils.NowToDateMvx())
			.replace("{5}", itno.trim())
			.replace("{6}", String.valueOf(priceReq).replace(",", "."))
			.replace("{7}", unms.trim())
			.replace("{4}",DateUtils.DateMvxToDateMvx(DateUtils.NowToDateMvx(), dayOffset))
			.replace("{8}", DateUtils.NowToDateMvx())
			.replace("{9}", DateUtils.NowToTimeMvx())
			.replace("{10}", orno.trim())
		;
		try {
			return emMvx.createNativeQuery(sqlT).executeUpdate();
		} catch (Exception ex) {
			LOG.info(ex.getMessage());
			return 0;
		}
	}

	@Transactional(REQUIRED)
	@ActivateRequestContext
	public int createOpriclForCunoItno(int cono, String cuno, String itno, BigDecimal priceReq, 
			String unms, int dayOffset,  String orno) {

		String sqlT = sqlExpressionService.getSqlExpressionEntry("Repo.createOpriclForCunoItno")
			.replace("{0}", String.valueOf(cono))
			.replace("{2}", "PLN")
			.replace("{1}", cuno.trim())
			.replace("{3}", DateUtils.NowToDateMvx())
			.replace("{5}", itno.trim())
			.replace("{6}", String.valueOf(priceReq).replace(",", "."))
			.replace("{7}", unms.trim())
			.replace("{4}",DateUtils.DateMvxToDateMvx(DateUtils.NowToDateMvx(), dayOffset))
			.replace("{8}", DateUtils.NowToDateMvx())
			.replace("{9}", DateUtils.NowToTimeMvx())
			.replace("{10}", orno.trim())
		;
		try {
			return emMvx.createNativeQuery(sqlT).executeUpdate();
		} catch (Exception ex) {
			LOG.info(ex.getMessage());
			return 0;
		}
	}
	@Transactional(NOT_SUPPORTED)
	@ActivateRequestContext
	public boolean checkObtype(int cono, String cuno, String ortp) {

		String sqlT = sqlExpressionService.getSqlExpressionEntry("Repo.checkObtype")
				.replace("{0}", cuno.trim())
				.replace("{1}", ortp.trim())
				;
		try {
			@SuppressWarnings("unchecked")
			List<EmInt> l = emMvx.createNativeQuery(sqlT, EmInt.class).getResultList();
			return l.get(0).getParVal().intValue() == 0 ? false : true;
		} catch (Exception ex) {
			LOG.info(ex.getMessage());
			return false;
		}
		
	}	

	@Transactional(NOT_SUPPORTED)
	@ActivateRequestContext
	public List<EmIntStr> orflowChkStatuses(int cono) {

		String sqlT = sqlExpressionService.getSqlExpressionEntry("Repo.orflowChkStatuses")
				.replace("{cono}", String.valueOf(cono))
				;
		try {
			@SuppressWarnings("unchecked")
			List<EmIntStr> l = emMvx.createNativeQuery(sqlT, EmIntStr.class).getResultList();
			return l;
		} catch (Exception ex) {
			LOG.info(ex.getMessage());
			return new ArrayList<EmIntStr>();
		}
		
	}	

	@Transactional(REQUIRED)
	@ActivateRequestContext
	public int createObtype(int cono, String cuno, String ortp) {

		String sqlT = sqlExpressionService.getSqlExpressionEntry("Repo.createObtype")
			.replace("{0}", cuno.trim())
			.replace("{1}", ortp.trim())
			.replace("{2}", DateUtils.NowToDateMvx())
			.replace("{3}", DateUtils.NowToTimeMvx())
		;
		try {
			return emMvx.createNativeQuery(sqlT).executeUpdate();
		} catch (Exception ex) {
			LOG.info(ex.getMessage());
			return 0;
		}
	}

	@Transactional(REQUIRED)
	@ActivateRequestContext
	public int orflowUpdateStatuses(int orunid, int ororst) {

		String sqlT = sqlExpressionService.getSqlExpressionEntry("Repo.orflowUpdateStatuses")
			.replace("{0}", String.valueOf(orunid))
			.replace("{1}", String.valueOf(ororst))
		;
		try {
			return emMvx.createNativeQuery(sqlT).executeUpdate();
		} catch (Exception ex) {
			LOG.info(ex.getMessage());
			return 0;
		}
	}

	//Repo.orflowUpdateStatuses
	@SuppressWarnings("unchecked")
	@Transactional(NOT_SUPPORTED)
	@ActivateRequestContext
	public int findOsytxhIdNext(int cono) {
		
		List<EmInt> l = emMvx.createNativeQuery(sqlExpressionService.getSqlExpressionEntry("Repo.findOsytxhIdNext")
				.replace("{cono}", String.valueOf(cono)) ,
				EmInt.class).getResultList();
		return l.size() > 0 ? l.get(0).getParVal().intValue() : 0;
	}

	@Transactional(REQUIRED)
	@ActivateRequestContext
	public int createOsytxh(int cono, int txid, String text, String orno) {

		String sqlT = sqlExpressionService.getSqlExpressionEntry("Repo.createOsytxh")
			.replace("{cono}", String.valueOf(cono))
			.replace("{0}", String.valueOf(txid))
			.replace("{1}", text.trim())
			.replace("{2}", orno.trim())
			.replace("{3}", DateUtils.NowToDateMvx())
			.replace("{4}", DateUtils.NowToTimeMvx())
		;
		try {
			return emMvx.createNativeQuery(sqlT).executeUpdate();
		} catch (Exception ex) {
			LOG.info(ex.getMessage());
			return 0;
		}
	}
	
	@Transactional(REQUIRED)
	@ActivateRequestContext
	public int createOsytxl(int cono, int txid, String text) {

		String sqlT = sqlExpressionService.getSqlExpressionEntry("Repo.createOsytxl")
			.replace("{cono}", String.valueOf(cono))
			.replace("{0}", String.valueOf(txid))
			.replace("{1}", text.trim())
		;
		try {
			return emMvx.createNativeQuery(sqlT).executeUpdate();
		} catch (Exception ex) {
			LOG.info(ex.getMessage());
			return 0;
		}
	}
	
	@Transactional(REQUIRED)
	@ActivateRequestContext
	public int updateOrderPrtx(int cono, int prtx, String orno) {
		
		String sqlT = sqlExpressionService.getSqlExpressionEntry("Repo.updateOrderPrtx")
				.replace("{cono}", String.valueOf(cono))
				.replace("{0}", String.valueOf(prtx))
				.replace("{1}", orno.trim())
			;
			try {
				return emMvx.createNativeQuery(sqlT).executeUpdate();
			} catch (Exception ex) {
				LOG.info(ex.getMessage());
				return 0;
			}
		
	}
	
	@Transactional(REQUIRED)
	@ActivateRequestContext
	public int createOrderChrg(int cono, String orno, String crid, String crd0, BigDecimal cram) {
		
		String sqlT = sqlExpressionService.getSqlExpressionEntry("Repo.createOrderChrg")
				.replace("{cono}", String.valueOf(cono))
				.replace("{0}", orno.trim())
				.replace("{1}", crid.trim())
				.replace("{2}", crd0.trim())
				.replace("{3}", new DecimalFormat("##.00").format(cram))
				;
			try {
				return emMvx.createNativeQuery(sqlT).executeUpdate();
			} catch (Exception ex) {
				LOG.info(ex.getMessage());
				return 0;
			}
		
	}

	@Transactional(REQUIRED)
	@ActivateRequestContext
	public int updateOrderHeaderAdd(int cono, String orno, String pycd, String tepy, String resp) {
		
		String sqlT = sqlExpressionService.getSqlExpressionEntry("Repo.updateOrderHeaderAdd")
				.replace("{cono}", String.valueOf(cono))
				.replace("{0}", orno.trim())
				.replace("{1}", pycd.trim())
				.replace("{2}", tepy.trim())
				.replace("{3}", resp.trim())
				;
			try {
				return emMvx.createNativeQuery(sqlT).executeUpdate();
			} catch (Exception ex) {
				LOG.info(ex.getMessage());
				return 0;
			}
		
	}

	@Transactional(REQUIRED)
	@ActivateRequestContext
	public int updateOrderHeaderOrtp(int cono, String orno) {
		
		String sqlT = sqlExpressionService.getSqlExpressionEntry("Repo.updateOrderHeadeOrtp")
				.replace("{cono}", String.valueOf(cono))
				.replace("{0}", orno.trim())
				;
			try {
				return emMvx.createNativeQuery(sqlT).executeUpdate();
			} catch (Exception ex) {
				LOG.info(ex.getMessage());
				return 0;
			}
		
	}

	@Transactional(NOT_SUPPORTED)
	@ActivateRequestContext
	public List<Ooline> getOolinesForOrnoSpecialPrice(int cono, String orno, String prrf) {

		String sqlT = sqlExpressionService.getSqlExpressionEntry("Repo.getOolinesForOrnoSpecialPrice")
				.replace("{cono}", String.valueOf(cono))
				.replace("{0}", orno.trim())
				.replace("{1}", prrf.trim())				;
		try {
			@SuppressWarnings("unchecked")
			List<Ooline> l = emMvx.createNativeQuery(sqlT, Ooline.class).getResultList();
			return l;
		} catch (Exception ex) {
			LOG.info(ex.getMessage());
			return new ArrayList<Ooline>();
		}
	}

	@Transactional(REQUIRED)
	@ActivateRequestContext
	public int updateOolinesForOrnoSpecialPrice(int cono, String orno, int ponr, BigDecimal sapr, 
			BigDecimal dia5, String prrf) {

		String sqlT = sqlExpressionService.getSqlExpressionEntry("Repo.updateOolinesForOrnoSpecialPrice")
			.replace("{cono}", String.valueOf(cono))
			.replace("{0}", orno.trim())
			.replace("{1}", String.valueOf(ponr))
			.replace("{2}", convBdToApi(sapr))
			.replace("{3}", convBdToApi(dia5))
			.replace("{4}", prrf.trim())
		;
		try {
			return emMvx.createNativeQuery(sqlT).executeUpdate();
		} catch (Exception ex) {
			LOG.info(ex.getMessage());
			return 0;
		}
	}

	@Transactional(REQUIRED)
	@ActivateRequestContext
	public int setWhlExt(int cono){

		String sqlT = sqlExpressionService.getSqlExpressionEntry("Repo.setWhlExt")
			.replace("{cono}", String.valueOf(cono))
		;
		try {
			return emMvx.createNativeQuery(sqlT).executeUpdate();
		} catch (Exception ex) {
			LOG.info(ex.getMessage());
			return 0;
		}
	}

	@Transactional(REQUIRED)
	@ActivateRequestContext
	public int setWhlInt(int cono){

		String sqlT = sqlExpressionService.getSqlExpressionEntry("Repo.setWhlInt")
			.replace("{cono}", String.valueOf(cono))
		;
		try {
			return emMvx.createNativeQuery(sqlT).executeUpdate();
		} catch (Exception ex) {
			LOG.info(ex.getMessage());
			return 0;
		}
	}

	@Transactional(REQUIRED)
	@ActivateRequestContext
	public int deleteOprbasForCunoChid(int cono, String chid){

		String sqlT = sqlExpressionService.getSqlExpressionEntry("Repo.deleteOprbasForCunoChid")
			.replace("{cono}", String.valueOf(cono))
			.replace("{0}", chid.trim())
		;
		try {
			return emMvx.createNativeQuery(sqlT).executeUpdate();
		} catch (Exception ex) {
			LOG.info(ex.getMessage());
			return 0;
		}
	}

	@Transactional(REQUIRED)
	@ActivateRequestContext
	public int deleteOpriclForCunoChid(int cono, String chid){

		String sqlT = sqlExpressionService.getSqlExpressionEntry("Repo.deleteOpriclForCunoChid")
			.replace("{cono}", String.valueOf(cono))
			.replace("{0}", chid.trim())
		;
		try {
			return emMvx.createNativeQuery(sqlT).executeUpdate();
		} catch (Exception ex) {
			LOG.info(ex.getMessage());
			return 0;
		}
	}

	
	@ActivateRequestContext
	public List<EmInt> getOrnoLinesForOrno(int cono, String orno, String whlo) {

		String sqlT = sqlExpressionService.getSqlExpressionEntry("Repo.getOrnoLinesForOrno")
			.replace("{cono}", String.valueOf(cono))
			.replace("{0}", orno.trim())
			.replace("{1}", whlo.trim())
		;
		try {
			@SuppressWarnings("unchecked")
			List<EmInt> l = emMvx.createNativeQuery(sqlT, EmInt.class).getResultList();
			return l;
		} catch (Exception ex) {
			LOG.info(ex.getMessage());
			return Collections.emptyList();
		}
	}
	
	@ActivateRequestContext
	public List<EmInt> getDlixToReleaseForOrno(int cono, String orno) {

		String sqlT = sqlExpressionService.getSqlExpressionEntry("Repo.getDlixToReleaseForOrno")
			.replace("{cono}", String.valueOf(cono))
			.replace("{0}", orno.trim())
		;
		try {
			@SuppressWarnings("unchecked")
			List<EmInt> l = emMvx.createNativeQuery(sqlT, EmInt.class).getResultList();
			return l;
		} catch (Exception ex) {
			LOG.info(ex.getMessage());
			return Collections.emptyList();
		}
	}

	@ActivateRequestContext
	public String findWhloForOrderLine(int cono, String itno) {
		
		String sqlT = sqlExpressionService.getSqlExpressionEntry("Repo.findWhloForOrderLine")
				.replace("{cono}", String.valueOf(cono))
				.replace("{0}", itno.trim())
			;
			try {
				@SuppressWarnings("unchecked")
				List<EmStr> l = emMvx.createNativeQuery(sqlT, EmStr.class).getResultList();
				return !l.isEmpty() ? l.get(0).getParVal() : "";
			} catch (Exception ex) {
				LOG.info(ex.getMessage());
				return "";
			}

	}
	
	@ActivateRequestContext
	public String getWhloForDlix(int cono, int dlix) {

		String sqlT = sqlExpressionService.getSqlExpressionEntry("Repo.getWhloForDlix")
			.replace("{cono}", String.valueOf(cono))
			.replace("{0}", String.valueOf(dlix))
		;
		try {
			@SuppressWarnings("unchecked")
			List<EmStr> l = emMvx.createNativeQuery(sqlT, EmStr.class).getResultList();
			return l.size() > 0 ? l.get(0).getParVal() : "";
		} catch (Exception ex) {
			LOG.info(ex.getMessage());
			return "";
		}
	}
	
	@ActivateRequestContext
	public List<Mhdisl> getMhdisl2(int cono, String orno) {

		String sqlT = sqlExpressionService.getSqlExpressionEntry("Repo.getMhdisl2")
			.replace("{cono}", String.valueOf(cono))
			.replace("{0}", String.valueOf(orno))
		;
		try {
			@SuppressWarnings("unchecked")
			List<Mhdisl> l = emMvx.createNativeQuery(sqlT, Mhdisl.class).getResultList();
			return l;
		} catch (Exception ex) {
			LOG.info(ex.getMessage());
			return new ArrayList<>();
		}
	}

	@Transactional(NOT_SUPPORTED)
	@ActivateRequestContext
	public Ocuscr3 getOcuscrForCuno(int cono, String cuno ) {
		
		String sqlT = sqlExpressionService.getSqlExpressionEntry("Repo.getOcuscrForCuno")
				.replace("{cono}", String.valueOf(cono))
				.replace("{0}", cuno.trim());
		try {
			var l = emMvx.createNativeQuery(sqlT, Ocuscr3.class).getResultList();
			return l.size() > 0 ? (Ocuscr3) l.get(0) : Ocuscr3.builder().build();
		} catch (Exception ex) {
			LOG.info(ex.getMessage());
			return Ocuscr3.builder().build();
		}
	}
	
	@Transactional(NOT_SUPPORTED)
	@ActivateRequestContext
	public String getPynoForCuno(int cono, String cuno ) {
		
		@SuppressWarnings("unchecked")
		List<EmStr> l = emMvx.createNativeQuery(sqlExpressionService.getSqlExpressionEntry("Repo.getPynoForCuno")
				.replace("{cono}", String.valueOf(cono))
				.replace("{0}", cuno.trim()),
				EmStr.class).getResultList();
		var pyno = l.size() > 0 ? l.get(0).getParVal().trim() : "";
		return  pyno;
	}

	@Transactional(NOT_SUPPORTED)
	@ActivateRequestContext
	public List<EmStr> findForeignOrders(int cono) {
		
		@SuppressWarnings("unchecked")
		List<EmStr> l = emMvx.createNativeQuery(sqlExpressionService.getSqlExpressionEntry("Repo.findForeignOrders")
				.replace("{cono}", String.valueOf(cono))
				,EmStr.class).getResultList();
		return  l;
	}
	
	@Transactional(NOT_SUPPORTED)
	@ActivateRequestContext
	public List<ProductPriceNew> getNewPriceForSafetyMargin(int cono) {
		
		@SuppressWarnings("unchecked")
		List<ProductPriceNew> l = emMvx.createNativeQuery(sqlExpressionService.getSqlExpressionEntry("Repo.getNewPriceForSafetyMargin")
				.replace("{cono}", String.valueOf(cono)),
				ProductPriceNew.class).getResultList();
		return  l;
	}

	@Transactional(NOT_SUPPORTED)
	@ActivateRequestContext
	public List<EmStr2Bd2> chkProductsAll(int cono) {
		
		@SuppressWarnings("unchecked")
		List<EmStr2Bd2> l = emMvx.createNativeQuery(sqlExpressionService.getSqlExpressionEntry("Repo.chkProductsAll")
				.replace("{cono}", String.valueOf(cono)),
				EmStr2Bd2.class).getResultList();
		return  l;
	}

	@Transactional(REQUIRED)
	@ActivateRequestContext
	public int updateZprice2(int cono, String itno, String whlo, BigDecimal price) {

		String sqlT = sqlExpressionService.getSqlExpressionEntry("Repo.updateZprice2")
			.replace("{cono}", String.valueOf(cono))
			.replace("{0}", itno.trim())
			.replace("{1}", whlo.trim())
			.replace("{2}", DateFormatter.dnf.format(price) )
		;	
		try {
			return emMvx.createNativeQuery(sqlT).executeUpdate();
		} catch (Exception ex) {
			LOG.info(ex.getMessage());
			return 0;
		}
	}

	@Transactional(REQUIRED)
	@ActivateRequestContext
	public int updateOrderForBlc(int cono, String orno, int code) {

		String sqlT = sqlExpressionService.getSqlExpressionEntry("Repo.updateOrderForBlc")
			.replace("{cono}", String.valueOf(cono))
			.replace("{0}", orno.trim())
			.replace("{1}", String.valueOf(code))
		;
		try {
			return emMvx.createNativeQuery(sqlT).executeUpdate();
		} catch (Exception ex) {
			LOG.info(ex.getMessage());
			return 0;
		}
	}
	
	@Transactional(REQUIRED)
	@ActivateRequestContext
	public int resetOcuscrForCuno(int cono, String cuno) {

		String sqlT = sqlExpressionService.getSqlExpressionEntry("Repo.resetOcuscrForCuno")
			.replace("{cono}", String.valueOf(cono))
			.replace("{0}", cuno.trim())
		;
		try {
			return emMvx.createNativeQuery(sqlT).executeUpdate();
		} catch (Exception ex) {
			LOG.info(ex.getMessage());
			return 0;
		}
	}

	@Transactional(REQUIRED)
	@ActivateRequestContext
	public int recreateOcuscrForCuno(int cono, String cuno, Ocuscr3 ocuscr) {

		String sqlT = sqlExpressionService.getSqlExpressionEntry("Repo.recreateOcuscrForCuno")
			.replace("{cono}", String.valueOf(cono))
			.replace("{0}", cuno.trim())
			.replace("{1}", convBdToApi(ocuscr.getOktdin()) )
			.replace("{2}", convBdToApi(ocuscr.getOktoin()) )
			.replace("{3}", convBdToApi(ocuscr.getOktblg()) )
			.replace("{4}", String.valueOf(ocuscr.getOkodue().intValue()) )
		;
		try {
			return emMvx.createNativeQuery(sqlT).executeUpdate();
		} catch (Exception ex) {
			LOG.info(ex.getMessage());
			return 0;
		}
	}
	
	@Transactional(REQUIRED)
	@ActivateRequestContext
	public int updateOcusmaSuplFields(int cono, String cuno, String alcu, String vrno, String resp) {

		String sqlT = sqlExpressionService.getSqlExpressionEntry("Repo.updateOcusmaSuplFields")
			.replace("{0}", String.valueOf(cono))
			.replace("{1}", cuno.trim())
			.replace("{2}", alcu.trim() )
			.replace("{3}", vrno.trim() )
			.replace("{4}", resp.trim() )
		;
		try {
			return emMvx.createNativeQuery(sqlT).executeUpdate();
		} catch (Exception ex) {
			LOG.info(ex.getMessage());
			return 0;
		}
	}

	@Transactional(REQUIRED)
	@ActivateRequestContext
	public int deleteShbal() {

		String sqlT = sqlExpressionService.getSqlExpressionEntry("Repo.deleteShbal")
		;
		try {
			return emMvx.createNativeQuery(sqlT).executeUpdate();
		} catch (Exception ex) {
			LOG.info(ex.getMessage());
			return 0;
		}
	}

	@Transactional(REQUIRED)
	@ActivateRequestContext
	public <T> T updateRec(T rec) {
		return emMvx.merge(rec);
	}

	@Transactional(REQUIRED)
	@ActivateRequestContext
	public <T> T createRec(T rec) {
		emMvx.persist(rec);
		return rec;
	}

	@ActivateRequestContext
	public String getMailTemplate(String name) {
		String templ =  sqlExpressionService.getSqlExpressionEntry("Repo.getMailTemplate" + name.trim());
		return templ;
	}

	public static String convBdToApi(BigDecimal value) {
		return value != null ? String.valueOf(value).replace(",", "."): "0";
	}

	
}

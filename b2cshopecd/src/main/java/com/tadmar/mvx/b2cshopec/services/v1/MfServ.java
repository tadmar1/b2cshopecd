package com.tadmar.mvx.b2cshopec.services.v1;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.core.Response;

import org.eclipse.microprofile.rest.client.inject.RestClient;

import com.tadmar.mvx.b2cshopec.model.mfserv.ResultObj;
import com.tadmar.mvx.b2cshopec.model.mfserv.CustData;
import com.tadmar.mvx.b2cshopec.rest.client.ApiExtV1ClientMf;

@ApplicationScoped
public class MfServ {

	@Inject @RestClient ApiExtV1ClientMf apiExtV1ClientMf;

	@PostConstruct
	void init() {}

	public Response searchCustomerByNip(String nip) {
		
		var date = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
		List<CustData> custDatas = new ArrayList<>();
		try {
			Response resp = apiExtV1ClientMf.searchCustomerByNip(nip, date);
			var respObj = resp.readEntity(ResultObj.class);
			resp.close();
			custDatas.add(respObj.getResult().getSubject());
		} catch(Exception ex) {
		}
		var respOut = Response.status(200).entity(custDatas);
		return respOut.build();
	}

	public List<CustData> searchCustomerByNipInt(String nip) {
		
		var date = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
		List<CustData> custDatas = new ArrayList<>();
		try {
			Response resp = apiExtV1ClientMf.searchCustomerByNip(nip, date);
			var respObj = resp.readEntity(ResultObj.class);
			resp.close();
			custDatas.add(respObj.getResult().getSubject());
		} catch(Exception ex) {
		}
		return custDatas;
	}

}

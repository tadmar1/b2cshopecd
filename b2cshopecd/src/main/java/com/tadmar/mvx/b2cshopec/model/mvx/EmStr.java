package com.tadmar.mvx.b2cshopec.model.mvx;

import javax.persistence.Entity;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Entity
@EqualsAndHashCode @ToString @NoArgsConstructor @AllArgsConstructor @Builder

public class EmStr {

    @Id
    private String parVal;

	public String getParVal() {
		return parVal;
	}
	public void setParVal(String parVal) {
		this.parVal = parVal;
	}
}

package com.tadmar.mvx.b2cshopec.model.b2c;

import java.io.Serializable;
import java.util.List;

import javax.json.bind.annotation.JsonbProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

@NoArgsConstructor @AllArgsConstructor @Builder
public class ParcelFList implements Serializable {

	private static final long serialVersionUID = 1L;

	@JsonbProperty("count")
	private Integer count;
	
	@JsonbProperty("pages")
	private Integer pages;

	@JsonbProperty("page")
	private Integer page;

	@JsonbProperty("list")
	private List<ParcelF> list;

	public Integer getCount() {
		return count;
	}
	public void setCount(Integer count) {
		this.count = count;
	}
	public Integer getPages() {
		return pages;
	}
	public void setPages(Integer pages) {
		this.pages = pages;
	}
	public Integer getPage() {
		return page;
	}
	public void setPage(Integer page) {
		this.page = page;
	}
	public List<ParcelF> getList() {
		return list;
	}
	public void setList(List<ParcelF> list) {
		this.list = list;
	}
	@Override
	public String toString() {
		return "ParcelFList [count=" + count + ", pages=" + pages + ", page=" + page + ", list=" + list + "]";
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((count == null) ? 0 : count.hashCode());
		result = prime * result + ((list == null) ? 0 : list.hashCode());
		result = prime * result + ((page == null) ? 0 : page.hashCode());
		result = prime * result + ((pages == null) ? 0 : pages.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ParcelFList other = (ParcelFList) obj;
		if (count == null) {
			if (other.count != null)
				return false;
		} else if (!count.equals(other.count))
			return false;
		if (list == null) {
			if (other.list != null)
				return false;
		} else if (!list.equals(other.list))
			return false;
		if (page == null) {
			if (other.page != null)
				return false;
		} else if (!page.equals(other.page))
			return false;
		if (pages == null) {
			if (other.pages != null)
				return false;
		} else if (!pages.equals(other.pages))
			return false;
		return true;
	}
}

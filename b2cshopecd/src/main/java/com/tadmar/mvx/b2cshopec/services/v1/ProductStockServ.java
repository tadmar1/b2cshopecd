package com.tadmar.mvx.b2cshopec.services.v1;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.WebApplicationException;

import org.eclipse.microprofile.rest.client.inject.RestClient;

import com.tadmar.mvx.b2cshopec.boot.ApplConfig;
import com.tadmar.mvx.b2cshopec.data.Repo;
import com.tadmar.mvx.b2cshopec.model.b2c.Product;
import com.tadmar.mvx.b2cshopec.model.b2c.ProductList;
import com.tadmar.mvx.b2cshopec.model.b2c.ProductSpOffers;
import com.tadmar.mvx.b2cshopec.model.b2c.ProductSpOffersList;
import com.tadmar.mvx.b2cshopec.model.b2c.ProductStock;
import com.tadmar.mvx.b2cshopec.model.b2c.ProductStockList;
import com.tadmar.mvx.b2cshopec.model.mvx.ProductStockPriceErp;
import com.tadmar.mvx.b2cshopec.model.mvx.Shbal;
import com.tadmar.mvx.b2cshopec.model.mvx.ShbalSpOffers;
import com.tadmar.mvx.b2cshopec.rest.client.ApiExtV1Client;

import io.quarkus.mailer.Mail;
import io.quarkus.mailer.Mailer;

import static com.tadmar.mvx.b2cshopec.boot.ApplConfig.*;

@ApplicationScoped
public class ProductStockServ {

	private static final Logger LOG = Logger.getLogger(ProductStockServ.class.getName());
	
	@Inject ApplConfig ac;
	@Inject Repo repo;
	@Inject @RestClient ApiExtV1Client apiExtV1Client;
	@Inject OrderServShp orderServShp;
	@Inject Mailer mailer;

	@PostConstruct
	void init() {}

	public void executeUpdateProductsAll(String userName) {

		try (var exec = Executors.newVirtualThreadPerTaskExecutor()) {
			exec.execute( () -> {
				chkProductsAll(userName);
				LOG.info("job updateProductsStocksQty :  started" );
				updateProductsStock(userName);
				LOG.info("job updateProductsStocksQty :  finished" );
				LOG.info("job updateProductPriceLowMargin :  started" );
				updateProductPriceLowMargin(userName);
				LOG.info("job UpdateProductPriceLowMargin :  finished" );
				LOG.info("job updateProductsStocksPrice :  started" );
				updateProductsStockPrice(userName);
				LOG.info("job updateProductsStocksPrice :  finished" );
				LOG.info("job updateProductsShbal :  started" );
				updateProductsShbal(userName);
				LOG.info("job updateProductsShbal :  finished" );
			});
		}	
	}

	public List<ProductStock> getProductsStock(String userName) {

		return getProductsStockPgs("");
	}

	public ProductStock getProductStock(int stockId, String userName) {
		
		ProductStock product = ProductStock.builder().build();
		try {
			var authKey = ac.getAuthKey();
			var response = apiExtV1Client.getProductStock(authKey, stockId);
			product = response.readEntity(ProductStock.class);
			response.close();
		} catch (WebApplicationException wae) {
			LOG.info("Products Stock Get error code for "  + wae.getResponse().getStatus());
		}
		return product;
	}

	public List<ShbalSpOffers> getSpecialoffers(String userName) {

		LOG.info("List<ProductSpOffers> " + userName + " " + Thread.currentThread().getName());
		List<ShbalSpOffers> productsOffers = new ArrayList<>();
		var productsSpOffers = getProductsSpOffersPgs(userName);
		var products = repo.getProductsShbalSpOffers(200);
		productsSpOffers.stream().forEach(pSp -> {
			var pfs = products.stream().filter(p -> p.getProdId().intValue() == pSp.getProductId().intValue()).collect(Collectors.toList());	
			for (var pf : pfs) {
				pf.setSpoffer(pSp.getDiscount());
				pf.setProffer(pf.getPrprice().subtract(pf.getSpoffer()));				
				productsOffers.add(pf);
			}
		});

		return productsOffers;
	}

	public List<Product> getProducts(String userName) {
		
		LOG.info("List<Product> " + userName + " " + Thread.currentThread().getName());
		return getProductsPgs("");
	}
	
	public void updateProductsStockPrice(String userName) {
	
		var pricesErp = repo.getProductsStockErp(MVX_COMPANY);
		var products = getProductsStockPgs(userName);
		
		pricesErp.stream().forEach(e -> {
			products.stream().filter(p-> p.getCode().trim().equals(e.getCode().trim()) && p.getPrice().compareTo(e.getPrice()) != 0).forEach(pf -> {
				LOG.info("FOR UPDATE:  " + pf.getStockId() + " " + pf.toString() );
				pf.setPrice(e.getPrice());
				updateProductsStock(pf);
			});
		});
		repo.updateProductsPriceErpStatusAll(MVX_COMPANY);
	}

	
	public void updateProductPriceLowMargin(String userName) {
		
		var prices = repo.getNewPriceForSafetyMargin(MVX_COMPANY);
		prices.stream().forEach(p -> {
			LOG.info("FOR UPDATE productPriceLowMargin: " + p.toString() + userName);	
			repo.updateZprice2(MVX_COMPANY, p.getZsitno().trim(), p.getZswhlo().trim(), p.getZsprbrn());
		});
	}
	
	public void updateProductsStock(String userName) {
		
		var productsErp = repo.getProductsStockErp(MVX_COMPANY);
		var productsErpBasket = computeStockWithBasket(productsErp, userName);
		var products = getProductsStockPgs(userName);

		products.stream().forEach(b -> {
			productsErpBasket.stream().filter(e -> e.getCode().trim().equals(b.getCode().trim()) && e.getStqt().intValue()!= b.getStock().intValue() )
				.forEach(u -> {
					if (u.getStqt().intValue() >= 0) {
						LOG.info("Update product " + b.getCode() + " " + b.getStock() + " " + u.getStqt());
						b.setStock(u.getStqt());
						updateProductsStock(b);
					}
				});
		});
	}

	public void updateProductsShbal(String userName) {
		
		var prods = getProductsPgs(userName);
		repo.deleteShbal();
		prods.stream().forEach(pr -> {
			var shbal = convertProductsShbal(pr);
			try {
				repo.createRec(shbal);
			} catch (Exception ex ) {
				LOG.info(shbal.toString());		
			}
		});
		chkProductsAll(userName);
	}	

	private List<ProductStockPriceErp> computeStockWithBasket(List<ProductStockPriceErp> productsErp, String userName) {
		
		/*
		var ordersBasket = new ArrayList<Order>();
		
		//ordersBasket = orderServShp.getOrders(SHP_ORD_REGISTERED, SHP_ORD_COMPLETED, userName);
		
		ordersBasket.stream().forEach(ob -> {
			var productsBasket = orderServShp.getOrderProducts(ob.getOrderId().intValue(), ob.getOrderId().intValue(), userName);
			productsBasket.forEach(pb -> {
				productsErp.stream().filter( pe -> pe.getCode().trim().equals(pb.getCode().trim())).forEach( pes ->{
					pes.setStqt(pes.getStqt() - pb.getQuantity());
					if (pes.getStqt().intValue() < 0 && pes.getStqt().intValue() != 1001) {
						pes.setStqt(0);
					}
					LOG.info("Shp basket: " + pes.getCode()+ " " + pes.getStqt() + " " + pb.getQuantity());
				});
			});
		});
		*/
		return productsErp;
	}
	
	private List<ProductStock> getProductsStockPgs(String userName) {
		
		List<ProductStock> products = new ArrayList<>();
		ProductStockList productsPage = null;

		var page = 1;
		while (true) {
			try {
				var response = apiExtV1Client.getProductStocks(ac.getAuthKey(), SHP_API_PAGE_SIZE, page);
				productsPage = response.readEntity(ProductStockList.class);
				response.close();
				if (productsPage.getList().isEmpty()) {
					break;
				}
				products.addAll(productsPage.getList());
				page ++;
			} catch (WebApplicationException wae) {
				LOG.info("Products Stocks Get error code for "  + wae.getResponse().getStatus());
				break;
			}
		}
		return products;
	}

	private List<ProductSpOffers> getProductsSpOffersPgs(String userName) {
		
		List<ProductSpOffers> products = new ArrayList<>();
		ProductSpOffersList productsPage = null;

		var page = 1;
		while (true) {
			try {
				var response = apiExtV1Client.getSpecialoffers(ac.getAuthKey(), SHP_API_PAGE_SIZE, page);
				productsPage = response.readEntity(ProductSpOffersList.class);
				response.close();
				if (productsPage.getList().isEmpty()) {
					break;
				}
				products.addAll(productsPage.getList());
				page ++;
			} catch (WebApplicationException wae) {
				LOG.info("Products SpOffres Get error code for "  + wae.getResponse().getStatus());
				break;
			}
		}
		return products;
	}


	private List<Product> getProductsPgs(String userName) {
		
		LOG.info("getProductsPgs " + Thread.currentThread().getName());

		List<Product> products = new ArrayList<>();
		ProductList productsPage = null;

		var page = 1;
		while (true) {
			try {
				var response = apiExtV1Client.getProducts(ac.getAuthKey(), SHP_API_PAGE_SIZE, page);
				productsPage = response.readEntity(ProductList.class);
				response.close();
				if (productsPage.getList().isEmpty()) {
					break;
				}
				products.addAll(productsPage.getList());
				page ++;
			} catch (WebApplicationException wae) {
				LOG.info("Products Get error code for "  + wae.getResponse().getStatus());
				break;
			}
		}
		return products;
	}

	private int updateProductsStock(ProductStock product) {
		
		var respStatus = 0;	

		while (true) {
			try {
				var response = apiExtV1Client.updateProductStock(ac.getAuthKey(), product.getStockId(), 
						product);
				@SuppressWarnings("unused")
				var respBody = response.readEntity(String.class);
				respStatus = response.getStatus();
				response.close();
				break;
			} catch (WebApplicationException wae) {
				LOG.info("Product Stock Update error code for "  + wae.getResponse().getStatus());
				break;
			}
		}	

		return respStatus;
	}

	private void chkProductsAll(String userName) {
		
		var df = new DecimalFormat();
		df.applyPattern("#.##");
		var chks = repo.chkProductsAll(MVX_COMPANY);
		if (!chks.isEmpty()) {
			var subject = "B2C Informacja o niezgodnych cenach produktów w Shoper ";
			var tableTempl31 = repo.getMailTemplate("ChkProductsAll31");
			var tableTempl32 = repo.getMailTemplate("ChkProductsAll32");

			var body = new StringBuilder();
			body.append(repo.getMailTemplate("ChkProductsAll1")).append(repo.getMailTemplate("ChkProductsAll2"));
			
			var row = 0;
			for ( var chk : chks) {
				var tableTempl3 = row % 2 == 0 ? tableTempl31 : tableTempl32;
				tableTempl3 = tableTempl3.replace("@PARVAL1", chk.getParVal1()).replace("@PARVAL2", chk.getParVal2())
						.replace("@PARVAL3", df.format(chk.getParVal3())).replace("@PARVAL4", df.format(chk.getParVal4()));
				body.append(tableTempl3);
				row ++;
			}
			body.append(repo.getMailTemplate("ChkProductsAll4"));
			
			var mail = Mail.withHtml(MGR_MAIL, subject, body.toString());
			mail.addBcc(ADMIN_MAIL);
			try {
				mailer.send(mail);
			} catch (Exception ex) {
				LOG.info(ex.getMessage());	
			}
		}
	}	

	
	private Shbal convertProductsShbal(Product prod) {
		
		var shbal = Shbal.builder()
		.active(prod.getTranslations().getTransloc().getActive())		
		.avalId(prod.getStock().getAvailabilityId())
		.calcui(prod.getStock().getCalculationUnitId())
		.calcur(prod.getStock().getCalculationUnitRatio())
		.code(prod.getStock().getCode())		
		.defaul(prod.getStock().getDefaultf())
		.extend(prod.getStock().getExtended())
		.packag(prod.getStock().getPackagef())
		.prcbuy(prod.getStock().getPriceBuying())
		.prcspe(prod.getStock().getPriceSpecial())
		.prcspt(prod.getStock().getPriceSpecialType())
		.prctyp(prod.getStock().getPriceType())
		.prcwho(prod.getStock().getPriceWholesale())
		.prcwht(prod.getStock().getPriceSpecialType())
		.prean(prod.getStock().getEan())
		.price(prod.getStock().getPrice())
		.prodId(prod.getProductId())
		.sold(prod.getStock().getSold())
		.stock(prod.getStock().getStockf())
		.stockId(prod.getStock().getStockId())
		.warlev(prod.getStock().getWarnLevel())
		.weight(prod.getStock().getWeight())
		.weityp(prod.getStock().getWeightType())
		.prname(prod.getTranslations().getTransloc().getName())
		.desful(prod.getTranslations().getTransloc().getDesful())
		.desshr(prod.getTranslations().getTransloc().getDesshr())		
		.attrib("")
		.attris(prod.getAttributes().toString())
		.dtAd(prod.getDtAd())
		.dtEd(prod.getDtEd())
		.gaugId(prod.getGaugId())
		.build();
		
		return shbal;
	}

}

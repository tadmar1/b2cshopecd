package com.tadmar.mvx.b2cshopec.services.v1.mvxapi;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Getter @Setter @EqualsAndHashCode @ToString @NoArgsConstructor @AllArgsConstructor @Builder

public class Tcerrm implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Id
	private String evunid;
	private int evcono;
	private String evdivi;
	private String evid01;
	private String evid02;
	private String evid03;
	private String evid04;
	private String evid05;
	private String evpgnm;
	private String evmsid;
	private int evlevl;
	private String evmdta;
	private int evrgnr;
}

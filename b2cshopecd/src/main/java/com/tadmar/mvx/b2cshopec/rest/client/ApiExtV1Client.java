package com.tadmar.mvx.b2cshopec.rest.client;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;

import org.eclipse.microprofile.faulttolerance.Retry;
import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;

import java.time.temporal.ChronoUnit;

import com.tadmar.mvx.b2cshopec.model.b2c.ParcelF;
import com.tadmar.mvx.b2cshopec.model.b2c.ProductStock;

@RegisterRestClient
@Path("/")
@Produces("application/json")
@Consumes("application/json")
@Retry(retryOn = WebApplicationException.class, delay = 1, delayUnit = ChronoUnit.SECONDS, maxRetries = 10)
public interface ApiExtV1Client {
	

	@Path("product-stocks")
	@GET
	Response getProductStocks(@HeaderParam("Authorization") String authKey, 
		@QueryParam("limit") int limit,
		@QueryParam("page") int page);

	@Path("products")
	@GET
	Response getProducts(@HeaderParam("Authorization") String authKey, 
		@QueryParam("limit") int limit,
		@QueryParam("page") int page);

	@Path("product-stocks/{stock_id}")
	@GET
	Response getProductStock(@HeaderParam("Authorization") String authKey, 
		@PathParam("stock_id") int stockId);

	@Path("product-stocks/{stock_id}")
	@PUT
	Response updateProductStock(@HeaderParam("Authorization") String authKey, 
			@PathParam("stock_id") Integer stockId,
			ProductStock productStock); 

	@Path("orders")
	@GET
	Response getOrders(@HeaderParam("Authorization") String authKey, 
		@QueryParam("limit") int limit,
		@QueryParam("page") int page,
		@QueryParam("filters") String filters);

	@Path("specialoffers")
	@GET
	Response getSpecialoffers(@HeaderParam("Authorization") String authKey, 
		@QueryParam("limit") int limit,
		@QueryParam("page") int page);

	@Path("parcels")
	@GET
	Response getParcels(@HeaderParam("Authorization") String authKey, 
		@QueryParam("limit") int limit,
		@QueryParam("page") int page,
		@QueryParam("filters") String filters);

	@Path("parcels/{parcel_id}")
	@PUT
	Response updateParcel(@HeaderParam("Authorization") String authKey, 
			@PathParam("parcel_id") Integer stockId,
			ParcelF parcelF); 

	@Path("order-products")
	@GET
	Response getOrderProducts(@HeaderParam("Authorization") String authKey, 
		@QueryParam("limit") int limit,
		@QueryParam("page") int page,
		@QueryParam("filters") String filters);

	@Path("GetShippingCarriers")
	@POST
	Response getDocDlnosSh(@HeaderParam("Authorization") String authKey, 
			String jsonParams); 
}

package com.tadmar.mvx.b2cshopec.model.mvx;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Getter @Setter @EqualsAndHashCode @ToString @NoArgsConstructor @AllArgsConstructor @Builder
public class ProductStockErp implements Serializable {
	
	private static final long serialVersionUID = 1L;
 
	@Id
	private String unid;
	private String whlo;
	private String itno;
	private String code;
	private Integer stqt;
}	

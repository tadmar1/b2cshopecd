package com.tadmar.mvx.b2cshopec.model.mvx;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Getter @Setter @EqualsAndHashCode @ToString @NoArgsConstructor @AllArgsConstructor @Builder
public class ProductPriceNew implements Serializable {

	
	private static final long serialVersionUID = 1L;
 
	@Id
	private String zsunid;
	private BigDecimal zsprbrn;
	private BigDecimal zsprntn;
	private String zsitno;
	private String zswhlo;
}

package com.tadmar.mvx.b2cshopec.model.mfserv;

import java.io.Serializable;
import java.util.List;

import javax.json.bind.annotation.JsonbNillable;

//import javax.json.bind.annotation.JsonbProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@EqualsAndHashCode @ToString @NoArgsConstructor @AllArgsConstructor @Builder
@JsonbNillable
public class CustData implements Serializable {

	private static final long serialVersionUID = 1L;

	public String name;
	public String nip;
	public String statusVat;
	public String regon;
	public String pesel;
	public String krs;
	public String residenceAddress;
	public String workingAddress;
	public List<Object> representatives;
	public List<Object> authorizedClerks;	
	public List<Object> partners;
	public String registrationLegalDate;
	public String registrationDenialBasis;
	public String registrationDenialDate;
	public String restorationBasis;
	public String restorationDate;
	public String removalBasis;
	public String removalDate;
	public List<String> accountNumbers;
	public boolean hasVirtualAccounts;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getNip() {
		return nip;
	}
	public void setNip(String nip) {
		this.nip = nip;
	}
	public String getStatusVat() {
		return statusVat;
	}
	public void setStatusVat(String statusVat) {
		this.statusVat = statusVat;
	}
	public String getRegon() {
		return regon;
	}
	public void setRegon(String regon) {
		this.regon = regon;
	}
	public String getPesel() {
		return pesel;
	}
	public void setPesel(String pesel) {
		this.pesel = pesel;
	}
	public String getKrs() {
		return krs;
	}
	public void setKrs(String krs) {
		this.krs = krs;
	}
	public String getResidenceAddress() {
		return residenceAddress;
	}
	public void setResidenceAddress(String residenceAddress) {
		this.residenceAddress = residenceAddress;
	}
	public String getWorkingAddress() {
		return workingAddress;
	}
	public void setWorkingAddress(String workingAddress) {
		this.workingAddress = workingAddress;
	}
	public List<Object> getRepresentatives() {
		return representatives;
	}
	public void setRepresentatives(List<Object> representatives) {
		this.representatives = representatives;
	}
	public List<Object> getAuthorizedClerks() {
		return authorizedClerks;
	}
	public void setAuthorizedClerks(List<Object> authorizedClerks) {
		this.authorizedClerks = authorizedClerks;
	}
	public List<Object> getPartners() {
		return partners;
	}
	public void setPartners(List<Object> partners) {
		this.partners = partners;
	}
	public String getRegistrationLegalDate() {
		return registrationLegalDate;
	}
	public void setRegistrationLegalDate(String registrationLegalDate) {
		this.registrationLegalDate = registrationLegalDate;
	}
	public String getRegistrationDenialBasis() {
		return registrationDenialBasis;
	}
	public void setRegistrationDenialBasis(String registrationDenialBasis) {
		this.registrationDenialBasis = registrationDenialBasis;
	}
	public String getRegistrationDenialDate() {
		return registrationDenialDate;
	}
	public void setRegistrationDenialDate(String registrationDenialDate) {
		this.registrationDenialDate = registrationDenialDate;
	}
	public String getRestorationBasis() {
		return restorationBasis;
	}
	public void setRestorationBasis(String restorationBasis) {
		this.restorationBasis = restorationBasis;
	}
	public String getRestorationDate() {
		return restorationDate;
	}
	public void setRestorationDate(String restorationDate) {
		this.restorationDate = restorationDate;
	}
	public String getRemovalBasis() {
		return removalBasis;
	}
	public void setRemovalBasis(String removalBasis) {
		this.removalBasis = removalBasis;
	}
	public String getRemovalDate() {
		return removalDate;
	}
	public void setRemovalDate(String removalDate) {
		this.removalDate = removalDate;
	}
	public List<String> getAccountNumbers() {
		return accountNumbers;
	}
	public void setAccountNumbers(List<String> accountNumbers) {
		this.accountNumbers = accountNumbers;
	}
	public boolean isHasVirtualAccounts() {
		return hasVirtualAccounts;
	}
	public void setHasVirtualAccounts(boolean hasVirtualAccounts) {
		this.hasVirtualAccounts = hasVirtualAccounts;
	}
}

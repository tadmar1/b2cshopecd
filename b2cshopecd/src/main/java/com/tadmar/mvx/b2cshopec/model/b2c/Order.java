package com.tadmar.mvx.b2cshopec.model.b2c;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.json.bind.annotation.JsonbNumberFormat;
import javax.json.bind.annotation.JsonbProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@EqualsAndHashCode @ToString @NoArgsConstructor @AllArgsConstructor @Builder
public class Order implements Serializable {

	private static final long serialVersionUID = 1L;

	@JsonbProperty("order_id")
	private Integer orderId;
	
	@JsonbProperty("user_id")
	private Integer userId;
	
	@JsonbProperty("date")
	private String date;
	
	@JsonbProperty("status_id")
	private Integer statusId;
	
	@JsonbProperty("shipping_id")
	private Integer shippingId;

	@JsonbProperty("shipping_cost")
	@JsonbNumberFormat(locale = "en_US", value = "#0.00")
	private BigDecimal shippingCost;
	
	@JsonbProperty("email")
	private String email;
	
	@JsonbProperty("delivery_address")
	Address deliveryAddress;  

	@JsonbProperty("billing_address")
	Address billingAddress;

	public Integer getOrderId() {
		return orderId;
	}
	public void setOrderId(Integer orderId) {
		this.orderId = orderId;
	}
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public Integer getStatusId() {
		return statusId;
	}
	public void setStatusId(Integer statusId) {
		this.statusId = statusId;
	}
	public Integer getShippingId() {
		return shippingId;
	}
	public void setShippingId(Integer shippingId) {
		this.shippingId = shippingId;
	}
	public BigDecimal getShippingCost() {
		return shippingCost;
	}
	public void setShippingCost(BigDecimal shippingCost) {
		this.shippingCost = shippingCost;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public Address getDeliveryAddress() {
		return deliveryAddress;
	}
	public void setDeliveryAddress(Address deliveryAddress) {
		this.deliveryAddress = deliveryAddress;
	}
	public Address getBillingAddress() {
		return billingAddress;
	}
	public void setBillingAddress(Address billingAddress) {
		this.billingAddress = billingAddress;
	}  
}

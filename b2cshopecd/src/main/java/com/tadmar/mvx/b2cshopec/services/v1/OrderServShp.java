package com.tadmar.mvx.b2cshopec.services.v1;

import static com.tadmar.mvx.b2cshopec.boot.ApplConfig.SHP_API_PAGE_SIZE;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.WebApplicationException;

import org.eclipse.microprofile.rest.client.inject.RestClient;

import com.tadmar.mvx.b2cshopec.boot.ApplConfig;
import com.tadmar.mvx.b2cshopec.data.Repo;
import com.tadmar.mvx.b2cshopec.model.b2c.Order;
import com.tadmar.mvx.b2cshopec.model.b2c.OrderF;
import com.tadmar.mvx.b2cshopec.model.b2c.OrderFList;
import com.tadmar.mvx.b2cshopec.model.b2c.OrderList;
import com.tadmar.mvx.b2cshopec.model.b2c.ParcelF;
import com.tadmar.mvx.b2cshopec.model.b2c.ParcelFList;
import com.tadmar.mvx.b2cshopec.model.b2c.ProductOrder;
import com.tadmar.mvx.b2cshopec.model.b2c.ProductOrderList;
import com.tadmar.mvx.b2cshopec.rest.client.ApiExtV1Client;

@ApplicationScoped
public class OrderServShp {
	
	private static final Logger LOG = Logger.getLogger(OrderServShp.class.getName());
	
	@Inject ApplConfig ac;
	@Inject Repo repo;
	@Inject @RestClient ApiExtV1Client apiExtV1Client;


	public List<OrderF> getOrdersF(int statusFrom, int statusTo, String userName) {
		
		String filters = "{\"status_id\":{\">=\":@1,\"<=\":@2} }"
				.replace("@1", String.valueOf(statusFrom)).replace("@2", String.valueOf(statusTo));
		List<OrderF> orders = new ArrayList<>();
		OrderFList ordersPage = null;

		var page = 1;
		while (true) {
			try {
				var response = apiExtV1Client.getOrders(ac.getAuthKey(), SHP_API_PAGE_SIZE, page, filters);
				ordersPage = response.readEntity(OrderFList.class);
				response.close();
				if (ordersPage.getList().isEmpty()) {
					break;
				}
				orders.addAll(ordersPage.getList());
				page ++;
			} catch (WebApplicationException wae) {
				LOG.info("Orders Get error code for "  + wae.getResponse().getStatus());
				break;
			}
		}
		return orders;
	}

	public List<ParcelF> getParcelsF(int orderId, String userName) {
		
		String filters = "{\"order_id\":{\">=\":@1} }"
				.replace("@1", String.valueOf(orderId));
		
		List<ParcelF> parcels = new ArrayList<>();
		ParcelFList parcelsPage = null;

		var page = 1;
		while (true) {
			try {
				var response = apiExtV1Client.getParcels(ac.getAuthKey(), SHP_API_PAGE_SIZE, page, filters);
				parcelsPage = response.readEntity(ParcelFList.class);
				response.close();
				if (parcelsPage.getList().isEmpty()) {
					break;
				}
				parcels.addAll(parcelsPage.getList());
				page ++;
			} catch (WebApplicationException wae) {
				LOG.info("Parcels Get error code for "  + wae.getResponse().getStatus());
				break;
			}
		}
		return parcels;
	}

	public List<Order> getOrders(int statusFrom, int statusTo, String userName) {
		
		String filters = "{\"status_id\":{\">=\":@1,\"<=\":@2} }"
				.replace("@1", String.valueOf(statusFrom)).replace("@2", String.valueOf(statusTo));
		List<Order> orders = new ArrayList<>();
		OrderList ordersPage = null;

		var page = 1;
		while (true) {
			try {
				var response = apiExtV1Client.getOrders(ac.getAuthKey(), SHP_API_PAGE_SIZE, page, filters);
				ordersPage = response.readEntity(OrderList.class);
				response.close();
				if (ordersPage.getList().isEmpty()) {
					break;
				}
				orders.addAll(ordersPage.getList());
				page ++;
			} catch (WebApplicationException wae) {
				LOG.info("Orders Get error code for "  + wae.getResponse().getStatus());
				break;
			}
		}
		return orders;
	}

	public List<ProductOrder> getOrderProducts(int orderIdFrom, int orderIdTo, String userName) {
		
		String filters = "{\"order_id\":{\">=\":@1,\"<=\":@2} }"
				.replace("@1", String.valueOf(orderIdFrom)).replace("@2", String.valueOf(orderIdTo));
		List<ProductOrder> products = new ArrayList<>();
		ProductOrderList productsPage = null;

		var page = 1;
		while (true) {
			try {
				var response = apiExtV1Client.getOrderProducts(ac.getAuthKey(), SHP_API_PAGE_SIZE, page, filters);
				productsPage = response.readEntity(ProductOrderList.class);
				response.close();
				if (productsPage.getList().isEmpty()) {
					break;
				}
				products.addAll(productsPage.getList());
				page ++;
			} catch (WebApplicationException wae) {
				LOG.info("Order Products Get error code for "  + wae.getResponse().getStatus());
				break;
			}
		}
		return products;
	}
	
}

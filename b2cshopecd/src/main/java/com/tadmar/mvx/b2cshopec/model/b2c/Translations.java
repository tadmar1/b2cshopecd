package com.tadmar.mvx.b2cshopec.model.b2c;

import java.io.Serializable;

import javax.json.bind.annotation.JsonbProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@EqualsAndHashCode @ToString @NoArgsConstructor @AllArgsConstructor @Builder
public class Translations implements Serializable {

	private static final long serialVersionUID = 1L;

	
	@JsonbProperty("pl_PL")
	private TransLoc transloc;


	public TransLoc getTransloc() {
		return transloc;
	}
	public void setTransloc(TransLoc transloc) {
		this.transloc = transloc;
	}

}

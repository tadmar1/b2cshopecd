package com.tadmar.mvx.b2cshopec.model.b2c;

import java.io.Serializable;

import javax.json.bind.annotation.JsonbProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@EqualsAndHashCode @ToString @NoArgsConstructor @AllArgsConstructor @Builder
public class TransLoc implements Serializable {

	private static final long serialVersionUID = 1L;

	
	@JsonbProperty("name")
	private String name;

	@JsonbProperty("short_description")
	private String desshr;
	
	@JsonbProperty("description")
	private String desful;
	
	@JsonbProperty("active")
	private Integer active;
	
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDesshr() {
		return desshr;
	}

	public void setDesshr(String desshr) {
		this.desshr = desshr;
	}

	public String getDesful() {
		return desful;
	}

	public void setDesful(String desful) {
		this.desful = desful;
	}

	public Integer getActive() {
		return active;
	}

	public void setActive(Integer active) {
		this.active = active;
	}

}

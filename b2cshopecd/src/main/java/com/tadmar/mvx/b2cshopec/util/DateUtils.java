package com.tadmar.mvx.b2cshopec.util;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.Period;
import java.time.format.DateTimeFormatter;

public class DateUtils {
	
	private static String DATE_MVX_PATTERN = "yyyyMMdd";
	private static String TIME_MVX_PATTERN = "HHmmss";
	private static String DATE_PATTERN = "yyyy-MM-dd";
	
	public static String DateMvxToDateString(String mvxDate, int offset) {

		final DateTimeFormatter mvxFormat = DateTimeFormatter.ofPattern(DATE_MVX_PATTERN);
		LocalDate localDate = LocalDate.parse(mvxDate, mvxFormat).plusDays(offset);
		
		return localDate.format(DateTimeFormatter.ofPattern(DATE_PATTERN));
	}

	public static String DateMvxToDateMvx(String mvxDate, int offset) {

		final DateTimeFormatter mvxFormat = DateTimeFormatter.ofPattern(DATE_MVX_PATTERN);
		LocalDate localDate = LocalDate.parse(mvxDate, mvxFormat).plusDays(offset);
		
		return localDate.format(DateTimeFormatter.ofPattern(DATE_MVX_PATTERN));
	}

	public static String monthFirstDayDateMvx(int monthOffset) {

		return LocalDate.now().plusMonths(monthOffset).withDayOfMonth(1).format(DateTimeFormatter.ofPattern(DATE_MVX_PATTERN));
	}
	
	public static String monthLastDayDateMvx(int monthOffset) {

		return LocalDate.now().plusMonths(monthOffset + 1).withDayOfMonth(1).minusDays(1)
				.format(DateTimeFormatter.ofPattern(DATE_MVX_PATTERN));
	}

	public static String firstDateOfPeriodDateMvx(String period) {
	
		return LocalDate.of(Integer.valueOf(period.substring(0,4)), Integer.valueOf(period.substring(5,6)), 1)
				.format(DateTimeFormatter.ofPattern(DATE_MVX_PATTERN));
	}
	
	public static String lastDateOfPeriodDateMvx(String period) {
		
		return LocalDate.of(Integer.valueOf(period.substring(0,4)), Integer.valueOf(period.substring(5,6)), 1)
				.plusMonths(1).minusDays(1)
				.format(DateTimeFormatter.ofPattern(DATE_MVX_PATTERN));
	}

	public static String periodFromDateMvx() {
	
		return LocalDate.now().format(DateTimeFormatter.ofPattern(DATE_MVX_PATTERN)).substring(0, 6);
	}
	
	public static String NowToDateMvx() {
		
		return LocalDate.now().format(DateTimeFormatter.ofPattern(DATE_MVX_PATTERN));
	}

	public static int NowToDateMvxI() {
		
		return Integer.valueOf(NowToDateMvx());
	}

	public static String NowToTimeMvx() {
		
		return LocalTime.now().format(DateTimeFormatter.ofPattern(TIME_MVX_PATTERN));
	}

	public static int NowToTimeMvxI() {
		
		return Integer.valueOf(NowToTimeMvx());
	}
	
	public static int DateDiffStr(String dt1, String dt2) {
		
		return Period.between(LocalDate.parse(dt1), LocalDate.parse(dt2)).getDays();
	}
}

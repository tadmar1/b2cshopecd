package com.tadmar.mvx.b2cshopec.model.b2c;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.json.bind.annotation.JsonbNumberFormat;
import javax.json.bind.annotation.JsonbProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@EqualsAndHashCode @ToString @NoArgsConstructor @AllArgsConstructor @Builder
public class Stock implements Serializable {

	private static final long serialVersionUID = 1L;

	
	@JsonbProperty("stock_id")
	private Integer stockId;

	@JsonbProperty("price")
	@JsonbNumberFormat(locale = "en_US", value = "#0.00")
	private BigDecimal price;
	
	@JsonbProperty("extended")
	private Integer extended;

	@JsonbProperty("price_type")
	private Integer priceType;
	
	@JsonbProperty("active")
	private Integer active;

	@JsonbProperty("default")
	private Integer defaultf;
	
	@JsonbProperty("stock")
	@JsonbNumberFormat(locale = "en_US", value = "#0.00")
	private BigDecimal stockf;
	
	@JsonbProperty("warn_level")
	private Integer warnLevel;
	
	@JsonbProperty("sold")
	@JsonbNumberFormat(locale = "en_US", value = "#0.00")
	private BigDecimal sold;

	@JsonbProperty("code")
	private String code;
	
	@JsonbProperty("ean")
	private String ean;
	
	@JsonbProperty("weight")
	@JsonbNumberFormat(locale = "en_US", value = "#0.00")
	private BigDecimal weight;

	@JsonbProperty("weight_type")
	private Integer weightType;
	
	@JsonbProperty("availability_id")
	private Integer availabilityId;

	@JsonbProperty("package")
	@JsonbNumberFormat(locale = "en_US", value = "#0.00")
	private BigDecimal packagef;

	@JsonbProperty("price_wholesale")
	@JsonbNumberFormat(locale = "en_US", value = "#0.00")
	private BigDecimal priceWholesale;

	@JsonbProperty("price_special")
	@JsonbNumberFormat(locale = "en_US", value = "#0.00")
	private BigDecimal priceSpecial;

	@JsonbProperty("price_wholesale_type")
	private Integer priceWholesaleType;

	@JsonbProperty("price_special_type")
	private Integer priceSpecialType;

	@JsonbProperty("price_buying")
	@JsonbNumberFormat(locale = "en_US", value = "#0.00")
	private BigDecimal priceBuying;

	@JsonbProperty("calculation_unit_id")
	private Integer calculationUnitId;

	@JsonbProperty("calculation_unit_ratio")
	@JsonbNumberFormat(locale = "en_US", value = "#0.00")
	private BigDecimal calculationUnitRatio;

	public Integer getStockId() {
		return stockId;
	}

	public void setStockId(Integer stockId) {
		this.stockId = stockId;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public Integer getExtended() {
		return extended;
	}

	public void setExtended(Integer extended) {
		this.extended = extended;
	}

	public Integer getPriceType() {
		return priceType;
	}

	public void setPriceType(Integer priceType) {
		this.priceType = priceType;
	}

	public Integer getActive() {
		return active;
	}

	public void setActive(Integer active) {
		this.active = active;
	}

	public Integer getDefaultf() {
		return defaultf;
	}

	public void setDefaultf(Integer defaultf) {
		this.defaultf = defaultf;
	}

	public BigDecimal getStockf() {
		return stockf;
	}

	public void setStockf(BigDecimal stockf) {
		this.stockf = stockf;
	}

	public Integer getWarnLevel() {
		return warnLevel;
	}

	public void setWarnLevel(Integer warnLevel) {
		this.warnLevel = warnLevel;
	}

	public BigDecimal getSold() {
		return sold;
	}

	public void setSold(BigDecimal sold) {
		this.sold = sold;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getEan() {
		return ean;
	}

	public void setEan(String ean) {
		this.ean = ean;
	}

	public BigDecimal getWeight() {
		return weight;
	}

	public void setWeight(BigDecimal weight) {
		this.weight = weight;
	}

	public Integer getWeightType() {
		return weightType;
	}

	public void setWeightType(Integer weightType) {
		this.weightType = weightType;
	}

	public Integer getAvailabilityId() {
		return availabilityId;
	}

	public void setAvailabilityId(Integer availabilityId) {
		this.availabilityId = availabilityId;
	}

	public BigDecimal getPackagef() {
		return packagef;
	}

	public void setPackagef(BigDecimal packagef) {
		this.packagef = packagef;
	}

	public BigDecimal getPriceWholesale() {
		return priceWholesale;
	}

	public void setPriceWholesale(BigDecimal priceWholesale) {
		this.priceWholesale = priceWholesale;
	}

	public BigDecimal getPriceSpecial() {
		return priceSpecial;
	}

	public void setPriceSpecial(BigDecimal priceSpecial) {
		this.priceSpecial = priceSpecial;
	}

	public Integer getPriceWholesaleType() {
		return priceWholesaleType;
	}

	public void setPriceWholesaleType(Integer priceWholesaleType) {
		this.priceWholesaleType = priceWholesaleType;
	}

	public Integer getPriceSpecialType() {
		return priceSpecialType;
	}

	public void setPriceSpecialType(Integer priceSpecialType) {
		this.priceSpecialType = priceSpecialType;
	}

	public BigDecimal getPriceBuying() {
		return priceBuying;
	}

	public void setPriceBuying(BigDecimal priceBuying) {
		this.priceBuying = priceBuying;
	}

	public Integer getCalculationUnitId() {
		return calculationUnitId;
	}

	public void setCalculationUnitId(Integer calculationUnitId) {
		this.calculationUnitId = calculationUnitId;
	}

	public BigDecimal getCalculationUnitRatio() {
		return calculationUnitRatio;
	}

	public void setCalculationUnitRatio(BigDecimal calculationUnitRatio) {
		this.calculationUnitRatio = calculationUnitRatio;
	}
}

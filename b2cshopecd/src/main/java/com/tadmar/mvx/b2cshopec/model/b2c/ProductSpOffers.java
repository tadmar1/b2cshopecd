package com.tadmar.mvx.b2cshopec.model.b2c;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.json.bind.annotation.JsonbProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@EqualsAndHashCode @ToString @NoArgsConstructor @AllArgsConstructor @Builder
public class ProductSpOffers implements Serializable {

	private static final long serialVersionUID = 1L;

	
	@JsonbProperty("promo_id")
	private Integer promoId;

	@JsonbProperty("discount")
	private BigDecimal discount;

	@JsonbProperty("discount_wholesale")
	private BigDecimal discountWholesale;
	
	@JsonbProperty("discount_special")
	private BigDecimal discountSpecial;

	@JsonbProperty("date_from")
	private String dateFrom;

	@JsonbProperty("date_to")
	private String dateTo;

	@JsonbProperty("product_id")
	private Integer productId;
	
	@JsonbProperty("product")
	private String product;

    public Integer getPromoId() {
        return promoId;
    }
    public void setPromoId(Integer promoId) {
        this.promoId = promoId;
    }
    public BigDecimal getDiscount() {
        return discount;
    }
    public void setDiscount(BigDecimal discount) {
        this.discount = discount;
    }
    public BigDecimal getDiscountWholesale() {
        return discountWholesale;
    }
    public void setDiscountWholesale(BigDecimal discountWholesale) {
        this.discountWholesale = discountWholesale;
    }
    public BigDecimal getDiscountSpecial() {
        return discountSpecial;
    }
    public void setDiscountSpecial(BigDecimal discountSpecial) {
        this.discountSpecial = discountSpecial;
    }
    public String getDateFrom() {
        return dateFrom;
    }
    public void setDateFrom(String dateFrom) {
        this.dateFrom = dateFrom;
    }
    public String getDateTo() {
        return dateTo;
    }
    public void setDateTo(String dateTo) {
        this.dateTo = dateTo;
    }
    public Integer getProductId() {
        return productId;
    }
    public void setProductId(Integer productId) {
        this.productId = productId;
    }
    public String getProduct() {
        return product;
    }
    public void setProduct(String product) {
        this.product = product;
    }

}

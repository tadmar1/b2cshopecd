package com.tadmar.mvx.b2cshopec.boot;

import java.util.logging.Logger;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

@WebListener
public class ServletConfig implements ServletContextListener {

	private static final Logger LOG = Logger.getLogger(ServletConfig.class.getName());
	
    @Override
    public void contextInitialized(ServletContextEvent sce)  {

    	var sctx = sce.getServletContext();
        sctx.addFilter("SecurityFilter", "waffle.servlet.NegotiateSecurityFilter");
        sctx.getFilterRegistration("SecurityFilter")
        	.addMappingForUrlPatterns(null, false, "/*");
    	LOG.info("Servlet Context initialized");
    }
}
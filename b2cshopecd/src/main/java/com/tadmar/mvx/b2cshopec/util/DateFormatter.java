package com.tadmar.mvx.b2cshopec.util;

import java.text.DecimalFormat;
import java.time.format.DateTimeFormatter;

public class DateFormatter {
	
	public static final DateTimeFormatter df = DateTimeFormatter.ofPattern("yyyy-MM-dd");
	public static final DateTimeFormatter tf = DateTimeFormatter.ofPattern("HH:mm:ss");
	public static final DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
	public static final DecimalFormat dnf = new DecimalFormat("######.00");


}

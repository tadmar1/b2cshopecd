package com.tadmar.mvx.b2cshopec.rest;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;
import org.eclipse.microprofile.openapi.annotations.servers.Servers;
import org.eclipse.microprofile.openapi.annotations.servers.Server;

@Servers( value = {
	@Server(url = "http://bit:9097/b2cshopec" )}
)
@ApplicationPath("/api")
public class ApiInit extends Application {}

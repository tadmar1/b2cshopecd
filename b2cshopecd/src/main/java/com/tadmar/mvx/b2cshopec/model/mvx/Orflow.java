package com.tadmar.mvx.b2cshopec.model.mvx;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table (name="ORFLOW", schema="B2CSHP")
@Getter @Setter @EqualsAndHashCode @ToString @NoArgsConstructor @AllArgsConstructor @Builder
public class Orflow implements Serializable {
	
	private static final long serialVersionUID = 1L;
 
	@Id @GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer orunid;
	private String ororno;
	private Integer ororst;
	private String orornx;
	private Integer ororsh;
	private Integer orstsh;
	private Integer orpaid;
	private String orpaidn;
	private Integer orspid;
	private String orspidn;
	private String orcuno;
	private String oradid;
	private BigDecimal orssum;
	private BigDecimal orschg;
	private String orpycd;
	private String ortepy;
	private Integer orrgdt;
	private Integer orrgtm;
	private Integer orlmdt;
	private Integer orlmtm;
	private String orpaad;
	private String orspad;
	private String ortxcd;
	private Integer orpaen;
	private String orcuma;
	private String orchid;
	
	private String ordlnu;
	private String orivno;

	private BigDecimal orcdam;
	private BigDecimal orisam;
	private String orpkst;
	private String orpkpo;
	
	private String orbaph;
	private String ordaph;
	
	private String orcnot;
	private Integer orpdfi;
}	

package com.tadmar.mvx.b2cshopec.services.v1;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.json.bind.Jsonb;
import javax.json.bind.JsonbBuilder;


import com.tadmar.mvx.b2cshopec.boot.ApplConfig;
import com.tadmar.mvx.b2cshopec.data.Repo;
import com.tadmar.mvx.b2cshopec.model.b2c.Address;
import com.tadmar.mvx.b2cshopec.model.mvx.CunoAdid;
import com.tadmar.mvx.b2cshopec.model.mvx.Orflow;
import com.tadmar.mvx.b2cshopec.services.v1.mvxapi.MvxApi;
import com.tadmar.mvx.b2cshopec.services.v1.mvxapi.MvxApiException;
import com.tadmar.mvx.b2cshopec.util.RestClientUtil;

import MvxAPI.MvxSockJ;

import static com.tadmar.mvx.b2cshopec.boot.ApplConfig.*;

@ApplicationScoped
public class CustomerServMvx implements Serializable {

	static final long serialVersionUID = 1L;

	static final Logger LOG = Logger.getLogger(CustomerServMvx.class.getName());
	
	public static final String MAX_CUNO = "T520500000";
	//"T520220000";
	//T520300000
	public static final String TRAN_CODE_CRS610 = "CRS610MI";
	public static final String TRAN_NAME_ADD = "Add";
	public static final String TRAN_NAME_CHANGE_ORDER_INFO = "ChgOrderInfo";
	public static final String TRAN_NAME_CHANGE_FINANCIAL = "ChgFinancial";
	public static final String TRAN_NAME_ADD_ADDRESS = "AddAddress";

	public static final String RESP_24 = "PSLA";
	public static final String RESP_NAME_24 = "Sławomir Latuszek";
	public static final String SDST_24 = "24";

	
	@Inject ApplConfig ac;
	@Inject Repo repo;
	@Inject MfServ mfServ;
	
	@SuppressWarnings("serial")
	public CunoAdid findOrCreateCustomerMvx(Orflow orflow) {
		
		
		Jsonb jsonb = JsonbBuilder.create();
		List<Address> billingAddressL = jsonb.fromJson(orflow.getOrpaad(), new ArrayList<Address>(){}.getClass().getGenericSuperclass());
		var billingAddress = billingAddressL.get(0);

		List<Address> deliveryAddressL = jsonb.fromJson(orflow.getOrspad(), new ArrayList<Address>(){}.getClass().getGenericSuperclass());
		var deliveryAddress = deliveryAddressL.get(0);
		
		var nip = orflow.getOrtxcd().trim();
		var cuno = "";
		
		if (!nip.isEmpty()) {
			cuno = findCunoForTxCd(nip.trim(), billingAddress);
		} else {
			cuno = findCunoDetByEmail(orflow.getOrcuma().trim(), billingAddress);
		}
		if (cuno.isEmpty() || cuno.startsWith("!")) {
			try {
				cuno = createCunoForBillingAddress(nip, orflow.getOrcuma().trim(), billingAddress);
			} catch (MvxApiException e) { 
				LOG.info("Create Customer ERROR: " + e.getMessage());			}
		}
		
		var adid = repo.findCustMvxNumberAdid(MVX_COMPANY, cuno, deliveryAddress.getStreet1().trim().toUpperCase(), deliveryAddress.getCity().trim().toUpperCase(),
				deliveryAddress.getPostcode().trim());
		if (adid.isEmpty()) {
			try {
				adid = createAdidForDeliveryAddress(MVX_COMPANY, cuno, deliveryAddress, orflow.getOrpaid());
			} catch (MvxApiException e) {
				LOG.info("Create Customer Adid ERROR: " + e.getMessage());	
			}
		}
		return CunoAdid.builder().cuno(cuno.trim()).adid(adid.trim()).build();
	}
	
	private String findCunoDetByEmail(String email, Address billingAddress) {
		
		var cuno = "";
		var orflows = repo.findOrflowByMail(email.trim());
		if (!orflows.isEmpty()) {
			var orflow = orflows.get(0);
			var ocusmas = repo.findOcusmaForCuno(MVX_COMPANY, orflow.getOrcuno().trim());
			if ( !ocusmas.isEmpty()) {
				var ocusma = ocusmas.get(0); 
				if (ocusma.getOkcua4().trim().equals((billingAddress.getPostcode().trim() + " " + billingAddress.getCity().trim()).toUpperCase() )) {
					cuno = ocusma.getOkcuno();
				} else {
					cuno = EMPTY_CUNO;
				}
			} else {
				cuno = EMPTY_CUNO;
			}
		} else {
			cuno = EMPTY_CUNO;
		}
		
		return cuno;
	}
	
	private String findCunoForTxCd(String txcd, Address billingAddress) {
		
		var cuno = "";
		if (txcd.trim().length() > 0 ) {
			var ocusmas = repo.findOcusmaForTxCd(MVX_COMPANY, txcd.trim());
			if ( ! ocusmas.isEmpty()) {
				var ocusma = ocusmas.get(0); 
				if (ocusma.getOkcua4().trim().equals((billingAddress.getPostcode().trim() + " " + billingAddress.getCity().trim()).toUpperCase() )) {
					cuno = ocusma.getOkcuno();
				}
			}
		}
		return cuno;
	}
	
	public String createAdidForDeliveryAddress(int cono, String cuno, Address address, int paymentId) throws MvxApiException {
		
		var adid = repo.getCustMvxNextNumberAdid(cono, cuno);
		var objMi = getObjMi(TRAN_CODE_CRS610);
		@SuppressWarnings("unused")
		var lastErr = "";

		String cunmSrc = address.getCompany().trim().isEmpty() ? address.getFirstname().trim() + " " + address.getLastname().trim() : address.getCompany().trim();
		var cua1 = cunmSrc.length() > 36 ? cunmSrc.substring(0, 36) : cunmSrc; 
		if (paymentId == 1) {
			cua1 = "TADMAR";
			address.setStreet1("TOROWA 2/4");
			address.setPostcode("61-315");
			address.setCity("POZNAŃ");
		}

		RestClientUtil.delay(2000);
		objMi.mvxClearFields();
		objMi.mvxSetField("CONO", String.valueOf(MVX_COMPANY)); 
		objMi.mvxSetField("CUNO", cuno.trim());
		objMi.mvxSetField("CUNM", "B2C " + address.getCity().trim().toUpperCase());
		objMi.mvxSetField("ADRT", "1");
		objMi.mvxSetField("ADID", adid.trim());
		objMi.mvxSetField("CUA1", cua1.trim().toUpperCase());
		objMi.mvxSetField("CUA2", "UL. " + address.getStreet1().trim().toUpperCase());
		objMi.mvxSetField("CUA3", address.getPostcode().trim() + " " + address.getCity().trim().toUpperCase());
		objMi.mvxSetField("CUA4", "");
		objMi.mvxSetField("PONO", address.getPostcode().trim());
		objMi.mvxSetField("MODL", "KKD" );
		objMi.mvxSetField("CSCD", "PL");
		RestClientUtil.delay(2000);

		try {
			objMi.mvxAccess(TRAN_NAME_ADD_ADDRESS);
			lastErr = objMi.mvxGetLastError();
			//if (lastErr != null && lastErr.length() > 0 ) { throw new MvxApiException(lastErr); }	
		} catch (Exception ex ) { 
			lastErr = objMi.mvxGetLastError(); 
			throw new MvxApiException(ex.getMessage()); 
		}
		objMi.mvxClose();
		RestClientUtil.delay(1000);
		return adid;
	}
	
	public String createCunoForBillingAddress(String nip, String email, Address address) throws MvxApiException {
		
		var mfStreet = "";
		var mfCity = "";
		var mfPono = "";
		
		if (! nip.isEmpty()) {
			var mfDatas = mfServ.searchCustomerByNipInt(nip);
			if ( mfDatas != null  && !mfDatas.isEmpty() && mfDatas.get(0) != null) {
				var ra = mfDatas.get(0).getResidenceAddress() != null ? mfDatas.get(0).getResidenceAddress() : mfDatas.get(0).getWorkingAddress();
				var raArr = ra.split(",");
				mfStreet = raArr[0].trim();
				mfCity = raArr[1].trim();
				mfPono = mfCity.split(" ")[0].trim();
			}
		}	
		
		var objMi = getObjMi(TRAN_CODE_CRS610);
		var lastErr = "";
		var cuno = repo.getCustMvxNextNumberT(MVX_COMPANY, MAX_CUNO);
		String cunmSrc = address.getCompany().trim().isEmpty() ? address.getFirstname().trim() + " " + address.getLastname().trim() : address.getCompany().trim();
		var cunm = cunmSrc.length() > 36 ? cunmSrc.substring(0, 36) : cunmSrc;
		var cua1 = "";
		var cua2 = "";
		var alcu = "";
		if ( cunmSrc.length() <= 36) {
			cua1 = cunmSrc;
		} else {
			cua1 = cunmSrc.substring(0, 36);
			cua2 = cunmSrc.substring(36).trim();
		}
		if (nip.isEmpty()) {
			alcu = address.getCompany().trim().isEmpty() ? address.getLastname().trim() : address.getCompany().trim();
			alcu = cunmSrc.length() > 10 ? cunmSrc.substring(0, 10) : cunmSrc;
		} else {
			alcu = nip;
		}
		

		objMi.mvxClearFields();
		objMi.mvxSetField("CUTM", nip.isEmpty() ? " TEMPLDET" : " TEMPLINN");
		objMi.mvxSetField("CONO", String.valueOf(MVX_COMPANY)); 
		objMi.mvxSetField("DIVI", "   ");
		objMi.mvxSetField("LNCD", "PL");
		objMi.mvxSetField("CSCD", "PL");
		objMi.mvxSetField("CUNO", cuno.trim()); 
		objMi.mvxSetField("CUNM", cunm.trim().toUpperCase());
		objMi.mvxSetField("CUA1", cua1.toUpperCase());
		objMi.mvxSetField("CUA2", cua2.toUpperCase());
		objMi.mvxSetField("CUA3", mfStreet.isEmpty() ? "UL. " + address.getStreet1().trim().toUpperCase() : "UL. " + mfStreet.toUpperCase() );
		objMi.mvxSetField("CUA4", mfCity.isEmpty() ? address.getPostcode().trim() + " " + address.getCity().trim().toUpperCase() : mfCity.toUpperCase());
		objMi.mvxSetField("PONO", mfPono.isEmpty() ? address.getPostcode().trim() : mfPono + " " + mfCity);
		objMi.mvxSetField("ALCU", alcu.toUpperCase());

		objMi.mvxSetField("PHNO", address.getPhone().trim());
		objMi.mvxSetField("PHN2", "");
		objMi.mvxSetField("TFNO", address.getTaxIdentificationNumber().trim());
		objMi.mvxSetField("CUTP", "0");
		objMi.mvxSetField("MAIL", email.trim());
		objMi.mvxSetField("STAT", "20");

		try {
			objMi.mvxAccess(TRAN_NAME_ADD);
			lastErr = objMi.mvxGetLastError();
			//if (lastErr != null && lastErr.length() > 0 ) { throw new MvxApiException(lastErr); }	
		} catch (Exception ex ) { 
			lastErr = objMi.mvxGetLastError(); 
			throw new MvxApiException(ex.getMessage()); 
		}
		objMi.mvxClose();
		
		objMi = getObjMi(TRAN_CODE_CRS610);
		objMi.mvxClearFields();
		objMi.mvxSetField("CONO", String.valueOf(MVX_COMPANY)); 
		objMi.mvxSetField("CUNO", cuno.trim()); 
		objMi.mvxSetField("VRNO", nip); 
		try {
			objMi.mvxAccess(TRAN_NAME_CHANGE_FINANCIAL);
			lastErr = objMi.mvxGetLastError();
			if (lastErr != null && lastErr.length() > 0 ) { throw new MvxApiException(lastErr); }	
		} catch (Exception ex ) { 
			lastErr = objMi.mvxGetLastError(); 
			throw new MvxApiException(ex.getMessage()); 
		}
		objMi.mvxClose();
		
		
		objMi = getObjMi(TRAN_CODE_CRS610);
		objMi.mvxClearFields();
		objMi.mvxSetField("CONO", String.valueOf(MVX_COMPANY)); 
		objMi.mvxSetField("CUNO", cuno.trim()); 
		objMi.mvxSetField("OREF", RESP_NAME_24.toUpperCase()); 
		objMi.mvxSetField("SMCD", RESP_24.toUpperCase()); 
		objMi.mvxSetField("SDST", SDST_24.toUpperCase()); 
		try {
			objMi.mvxAccess(TRAN_NAME_CHANGE_ORDER_INFO);
			lastErr = objMi.mvxGetLastError();
			if (lastErr != null && lastErr.length() > 0 ) { throw new MvxApiException(lastErr); }	
		} catch (Exception ex ) { 
			lastErr = objMi.mvxGetLastError(); 
			throw new MvxApiException(ex.getMessage()); 
		}
		objMi.mvxClose();
		
		repo.updateOcusmaSuplFields(MVX_COMPANY, cuno, alcu.toUpperCase(), nip, RESP_24.toUpperCase() );
		
		return cuno;
	}
	
	private MvxSockJ getObjMi(String trnCode) {
		
		var apiMi = new MvxApi(trnCode);
		apiMi.connect();
		var objMi = apiMi.getApiObj();
		objMi.TRIM = true; 
		objMi.DEBUG = true;
		return objMi;
	}
}

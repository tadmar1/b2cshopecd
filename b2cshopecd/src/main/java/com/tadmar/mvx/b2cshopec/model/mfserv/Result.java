package com.tadmar.mvx.b2cshopec.model.mfserv;

import java.io.Serializable;

import javax.json.bind.annotation.JsonbNillable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@JsonbNillable
@EqualsAndHashCode @ToString @NoArgsConstructor @AllArgsConstructor @Builder
public class Result implements Serializable {

	private static final long serialVersionUID = 1L;

	public CustData subject;
	public String reuestId;
	public String requestDateTime;
	public CustData getSubject() {
		return subject;
	}
	public void setSubject(CustData subject) {
		this.subject = subject;
	}
	public String getReuestId() {
		return reuestId;
	}
	public void setReuestId(String reuestId) {
		this.reuestId = reuestId;
	}
	public String getRequestDateTime() {
		return requestDateTime;
	}
	public void setRequestDateTime(String requestDateTime) {
		this.requestDateTime = requestDateTime;
	}
}

package com.tadmar.mvx.b2cshopec.model.b2c;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.json.bind.annotation.JsonbNumberFormat;
import javax.json.bind.annotation.JsonbProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@EqualsAndHashCode @ToString @NoArgsConstructor @AllArgsConstructor @Builder
public class ProductOrder implements Serializable {

	private static final long serialVersionUID = 1L;

	@JsonbProperty("id")
	private Integer Id;
	
	@JsonbProperty("order_id")
	private Integer orderId;
	
	@JsonbProperty("product_id")
	private Integer productId;

	@JsonbProperty("stock_id")
	private Integer stockId;
	
	@JsonbProperty("price")
	@JsonbNumberFormat(locale = "en_US", value = "#0.00")
	private BigDecimal price;

	@JsonbProperty("quantity")
	private Integer quantity;

	@JsonbProperty("name")
	private String name;

	@JsonbProperty("code")
	private String code;

	@JsonbProperty("unit")
	private String unit;

	@JsonbProperty("tax_value")
	private String taxValue;

	@JsonbProperty("discount_perc")
	private String discountPerc;

	public Integer getId() {
		return Id;
	}

	public void setId(Integer id) {
		Id = id;
	}

	public Integer getOrderId() {
		return orderId;
	}

	public void setOrderId(Integer orderId) {
		this.orderId = orderId;
	}

	public Integer getProductId() {
		return productId;
	}

	public void setProductId(Integer productId) {
		this.productId = productId;
	}

	public Integer getStockId() {
		return stockId;
	}

	public void setStockId(Integer stockId) {
		this.stockId = stockId;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public String getTaxValue() {
		return taxValue;
	}

	public void setTaxValue(String taxValue) {
		this.taxValue = taxValue;
	}

	public String getDiscountPerc() {
		return discountPerc;
	}

	public void setDiscountPerc(String discountPerc) {
		this.discountPerc = discountPerc;
	}
}

package com.tadmar.mvx.b2cshopec.model.b2c;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import javax.json.bind.annotation.JsonbNumberFormat;
import javax.json.bind.annotation.JsonbProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

@NoArgsConstructor @AllArgsConstructor @Builder
public class OrderF implements Serializable {

	private static final long serialVersionUID = 1L;

	@JsonbProperty("order_id")
	private Integer orderId;
	
	@JsonbProperty("user_id")
	private Integer userId;
	
	@JsonbProperty("date")
	private String date;
	
	@JsonbProperty("status_id")
	private Integer statusId;
	
	@JsonbProperty("shipping_id")
	private Integer shippingId;

	@JsonbProperty("shipping_cost")
	@JsonbNumberFormat(locale = "en_US", value = "#0.00")
	private BigDecimal shippingCost;
	
	@JsonbProperty("email")
	private String email;
	
	@JsonbProperty("delivery_address")
	Address deliveryAddress;  

	@JsonbProperty("billing_address")
	Address billingAddress;

	@JsonbProperty("sum")
	@JsonbNumberFormat(locale = "en_US", value = "#0.00")
	private BigDecimal sum;
	
	@JsonbProperty("payment_id")
	private Integer paymentId;
	
	@JsonbProperty("pickup_point")
	private String pickupPoint;

	@JsonbProperty("notes")
	private String notes;

	@JsonbProperty("additional_fields")
	private List<OrderFAdd> additionalFields;

	public Integer getOrderId() {
		return orderId;
	}

	public void setOrderId(Integer orderId) {
		this.orderId = orderId;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public Integer getStatusId() {
		return statusId;
	}

	public void setStatusId(Integer statusId) {
		this.statusId = statusId;
	}

	public Integer getShippingId() {
		return shippingId;
	}

	public void setShippingId(Integer shippingId) {
		this.shippingId = shippingId;
	}

	public BigDecimal getShippingCost() {
		return shippingCost;
	}

	public void setShippingCost(BigDecimal shippingCost) {
		this.shippingCost = shippingCost;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Address getDeliveryAddress() {
		return deliveryAddress;
	}

	public void setDeliveryAddress(Address deliveryAddress) {
		this.deliveryAddress = deliveryAddress;
	}

	public Address getBillingAddress() {
		return billingAddress;
	}

	public void setBillingAddress(Address billingAddress) {
		this.billingAddress = billingAddress;
	}

	public BigDecimal getSum() {
		return sum;
	}

	public void setSum(BigDecimal sum) {
		this.sum = sum;
	}

	public Integer getPaymentId() {
		return paymentId;
	}

	public void setPaymentId(Integer paymentId) {
		this.paymentId = paymentId;
	}

	public String getPickupPoint() {
		return pickupPoint;
	}

	public void setPickupPoint(String pickupPoint) {
		this.pickupPoint = pickupPoint;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public List<OrderFAdd> getAdditionalFields() {
		return additionalFields;
	}

	public void setAdditionalFields(List<OrderFAdd> additionalFields) {
		this.additionalFields = additionalFields;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((additionalFields == null) ? 0 : additionalFields.hashCode());
		result = prime * result + ((billingAddress == null) ? 0 : billingAddress.hashCode());
		result = prime * result + ((date == null) ? 0 : date.hashCode());
		result = prime * result + ((deliveryAddress == null) ? 0 : deliveryAddress.hashCode());
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result + ((notes == null) ? 0 : notes.hashCode());
		result = prime * result + ((orderId == null) ? 0 : orderId.hashCode());
		result = prime * result + ((paymentId == null) ? 0 : paymentId.hashCode());
		result = prime * result + ((pickupPoint == null) ? 0 : pickupPoint.hashCode());
		result = prime * result + ((shippingCost == null) ? 0 : shippingCost.hashCode());
		result = prime * result + ((shippingId == null) ? 0 : shippingId.hashCode());
		result = prime * result + ((statusId == null) ? 0 : statusId.hashCode());
		result = prime * result + ((sum == null) ? 0 : sum.hashCode());
		result = prime * result + ((userId == null) ? 0 : userId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		OrderF other = (OrderF) obj;
		if (additionalFields == null) {
			if (other.additionalFields != null)
				return false;
		} else if (!additionalFields.equals(other.additionalFields))
			return false;
		if (billingAddress == null) {
			if (other.billingAddress != null)
				return false;
		} else if (!billingAddress.equals(other.billingAddress))
			return false;
		if (date == null) {
			if (other.date != null)
				return false;
		} else if (!date.equals(other.date))
			return false;
		if (deliveryAddress == null) {
			if (other.deliveryAddress != null)
				return false;
		} else if (!deliveryAddress.equals(other.deliveryAddress))
			return false;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		if (notes == null) {
			if (other.notes != null)
				return false;
		} else if (!notes.equals(other.notes))
			return false;
		if (orderId == null) {
			if (other.orderId != null)
				return false;
		} else if (!orderId.equals(other.orderId))
			return false;
		if (paymentId == null) {
			if (other.paymentId != null)
				return false;
		} else if (!paymentId.equals(other.paymentId))
			return false;
		if (pickupPoint == null) {
			if (other.pickupPoint != null)
				return false;
		} else if (!pickupPoint.equals(other.pickupPoint))
			return false;
		if (shippingCost == null) {
			if (other.shippingCost != null)
				return false;
		} else if (!shippingCost.equals(other.shippingCost))
			return false;
		if (shippingId == null) {
			if (other.shippingId != null)
				return false;
		} else if (!shippingId.equals(other.shippingId))
			return false;
		if (statusId == null) {
			if (other.statusId != null)
				return false;
		} else if (!statusId.equals(other.statusId))
			return false;
		if (sum == null) {
			if (other.sum != null)
				return false;
		} else if (!sum.equals(other.sum))
			return false;
		if (userId == null) {
			if (other.userId != null)
				return false;
		} else if (!userId.equals(other.userId))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "OrderF [orderId=" + orderId + ", userId=" + userId + ", date=" + date + ", statusId=" + statusId
				+ ", shippingId=" + shippingId + ", shippingCost=" + shippingCost + ", email=" + email
				+ ", deliveryAddress=" + deliveryAddress + ", billingAddress=" + billingAddress + ", sum=" + sum
				+ ", paymentId=" + paymentId + ", pickupPoint=" + pickupPoint + ", notes=" + notes
				+ ", additionalFields=" + additionalFields + "]";
	}
	

}

package com.tadmar.mvx.b2cshopec.model.mvx;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Getter @Setter @EqualsAndHashCode @ToString @NoArgsConstructor @AllArgsConstructor @Builder
public class Ocuscr3 implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id 
	private String okcuno;
	private BigDecimal okcrlm;
	private BigDecimal okcrl2;
	private BigDecimal okcrl3;
	private Integer okodud;
	private BigDecimal oktdin;
	private BigDecimal oktoin;
	private BigDecimal oktblg;
	private Integer okodue;
}

package com.tadmar.mvx.b2cshopec.model.cdpserv;

import java.io.Serializable;

import javax.json.bind.annotation.JsonbNillable;
import javax.json.bind.annotation.JsonbProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@JsonbNillable
@EqualsAndHashCode @ToString @NoArgsConstructor @AllArgsConstructor @Builder
public class DocDlnu implements Serializable {

	private static final long serialVersionUID = 1L;

	@JsonbProperty("Id")
	public String id;

	@JsonbProperty("ExternalId")
	public String externalId;

	@JsonbProperty("ShippingOrder.ShippingNumber")
	public String shippingNumber;

	@JsonbProperty("Number")
	public String number;
	
	@JsonbProperty("DocumentLK")
	public String documentLk;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getExternalId() {
		return externalId;
	}

	public void setExternalId(String externalId) {
		this.externalId = externalId;
	}

	public String getShippingNumber() {
		return shippingNumber;
	}

	public void setShippingNumber(String shippingNumber) {
		this.shippingNumber = shippingNumber;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public String getDocumentLk() {
		return documentLk;
	}

	public void setDocumentLk(String documentLk) {
		this.documentLk = documentLk;
	}
}

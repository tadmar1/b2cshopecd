package com.tadmar.mvx.b2cshopec.model.mfserv;

import java.io.Serializable;

import javax.json.bind.annotation.JsonbNillable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@JsonbNillable
@EqualsAndHashCode @ToString @NoArgsConstructor @AllArgsConstructor @Builder
public class ResultObj implements Serializable {

	private static final long serialVersionUID = 1L;

	public Result result;

	public Result getResult() {
		return result;
	}

	public void setResult(Result result) {
		this.result = result;
	}
}

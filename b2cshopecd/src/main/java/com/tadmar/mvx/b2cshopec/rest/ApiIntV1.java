package com.tadmar.mvx.b2cshopec.rest;

import java.time.LocalDateTime;
import java.util.List;
import java.util.logging.Logger;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.json.Json;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

import org.jboss.resteasy.annotations.cache.NoCache;

import com.tadmar.mvx.b2cshopec.boot.ApplConfig;
import com.tadmar.mvx.b2cshopec.boot.B2cApiConfig;
import com.tadmar.mvx.b2cshopec.model.b2c.Product;
import com.tadmar.mvx.b2cshopec.services.v1.MfServ;
import com.tadmar.mvx.b2cshopec.services.v1.OrderServ;
import com.tadmar.mvx.b2cshopec.services.v1.OrderServShp;
import com.tadmar.mvx.b2cshopec.services.v1.ProductStockServ;

import io.smallrye.common.annotation.RunOnVirtualThread;

@Path("/int/v1")
@ApplicationScoped
@Produces("application/json")
@Consumes("application/json")
@NoCache
public class ApiIntV1 {
	
	public static final Logger LOG = Logger.getLogger(ApiIntV1.class.getName());

	@Inject ApplConfig ac;
	@Inject B2cApiConfig b2cApiConfig;
	@Inject ProductStockServ productStockServ;
	@Inject OrderServ orderServ;
	@Inject OrderServShp orderServShp;
	@Inject MfServ mfServ;
	
	@Path("product-stocks")
	@GET
	@RunOnVirtualThread
	public Response getProductStockAll(@Context SecurityContext sec) {
		Response.ResponseBuilder builder = null;
		try {
			var products = productStockServ.getProductsStock(getDname(sec));
			builder =  Response.ok().entity(products);
		} catch ( Exception ex ) {
			builder = Response.status(Response.Status.BAD_REQUEST).entity(ex);			
		}
		return builder.build();
	}

	@Path("products")
	@RunOnVirtualThread
	@GET
	public Response getProducts(@Context SecurityContext sec) {
		Response.ResponseBuilder builder = null;
		try {
			LOG.info("Response getProduct "  + Thread.currentThread().getName());
			List<Product> product = productStockServ.getProducts(getDname(sec));
			builder =  Response.ok().entity(product);
		} catch ( Exception ex ) {
			builder = Response.status(Response.Status.BAD_REQUEST).entity(ex);			
		}
		return builder.build();
	}

	@Path("product-stocks/{stock_id}")
	@RunOnVirtualThread
	@GET
	public Response getProductStock(@Context SecurityContext sec,@PathParam("stock_id") int stockId){
		
        Response.ResponseBuilder builder = null;
		try {
			var product = productStockServ.getProductStock(stockId, getDname(sec));
			builder =  Response.ok().entity(product);
		} catch ( Exception ex ) {
			builder = Response.status(Response.Status.BAD_REQUEST).entity(ex);			
		}
		return builder.build();
	}

	@Path("specialoffers")
	//@RunOnVirtualThread
	@GET
	public Response getSpecialoffers(@Context SecurityContext sec){
		
        Response.ResponseBuilder builder = null;
		try {
			var product = productStockServ.getSpecialoffers(getDname(sec));
			builder =  Response.ok().entity(product);
		} catch ( Exception ex ) {
			builder = Response.status(Response.Status.BAD_REQUEST).entity(ex);			
		}
		return builder.build();
	}


	@Path("orders")
	@RunOnVirtualThread
	@GET
	public Response getOrders(@Context SecurityContext sec,
			@QueryParam("status_from") int statusFrom,
			@QueryParam("status_to") int statusTo){
		
        Response.ResponseBuilder builder = null;
		try {
			var orders = orderServShp.getOrders(statusFrom, statusTo, getDname(sec));
			builder =  Response.ok().entity(orders);
		} catch ( Exception ex ) {
			builder = Response.status(Response.Status.BAD_REQUEST).entity(ex);			
		}
		return builder.build();
	}

	@Path("order-products")
	@RunOnVirtualThread
	@GET
	public Response getOrderProducts(@Context SecurityContext sec,
			@QueryParam("order_id_from") int orderIdFrom,
			@QueryParam("order_id_to") int orderIdTo){
		
        Response.ResponseBuilder builder = null;
		try {
			var orders = orderServShp.getOrderProducts(orderIdFrom, orderIdTo, getDname(sec));
			builder =  Response.ok().entity(orders);
		} catch ( Exception ex ) {
			builder = Response.status(Response.Status.BAD_REQUEST).entity(ex);			
		}
		return builder.build();
	}

	@Path("products-update-all")
	@GET
	public Response updateProductsAll(@Context SecurityContext sec) {
		Response.ResponseBuilder builder = null;
		try {
			productStockServ.executeUpdateProductsAll(getDname(sec));
			builder =  Response.ok().entity("Job UpdateProductsShbal submited " + LocalDateTime.now());
		} catch ( Exception ex ) {
			builder = Response.status(Response.Status.BAD_REQUEST).entity(ex);			
		}
		return builder.build();
	}

	@Path("products-shbal")
	@RunOnVirtualThread
	@GET
	public Response updateProductsShbal(@Context SecurityContext sec) {
		Response.ResponseBuilder builder = null;
		try {
			LOG.info("job updateProductsShbal :  started" );
			productStockServ.updateProductsShbal(getDname(sec));
			LOG.info("job updateProductsShbal :  finished" );
			builder =  Response.ok().entity("Job UpdateProductsShbal executed " + LocalDateTime.now());
		} catch ( Exception ex ) {
			builder = Response.status(Response.Status.BAD_REQUEST).entity(ex);			
		}
		return builder.build();
	}

	@Path("product-stocks-update-qty")
	@RunOnVirtualThread
	@GET
	public Response updateProductStockAll(@Context SecurityContext sec) {

		Response.ResponseBuilder builder = null;
		try {
			LOG.info("job updateProductsStocksQty :  started" );
			productStockServ.updateProductsStock(getDname(sec));
			LOG.info("job updateProductsStocksQty :  finished" );
			builder =  Response.ok().entity("Job UpdateProductsStockQty executed" + LocalDateTime.now());
		} catch ( Exception ex ) {
			builder = Response.status(Response.Status.BAD_REQUEST).entity(ex);			
		}
		return builder.build();
	}

	
	@Path("product-stocks-update-price")
	@RunOnVirtualThread
	@GET
	public Response updateProductPrice(@Context SecurityContext sec) {

		Response.ResponseBuilder builder = null;
		try {
			LOG.info("job updateProductsStocksPrice :  started" );
			productStockServ.updateProductsStockPrice(getDname(sec));
			LOG.info("job updateProductsStocksPrice :  finished" );
			builder =  Response.ok().entity("Job UpdateProductsStockPrice executed " + LocalDateTime.now());
		} catch ( Exception ex ) {
			builder = Response.status(Response.Status.BAD_REQUEST).entity(ex);			
		}
		return builder.build();
	}
	
	@Path("product-stocks-update-price-lowmargin")
	@RunOnVirtualThread
	@GET
	public Response updateProductPriceLowMargin(@Context SecurityContext sec) {

		Response.ResponseBuilder builder = null;
		try {
			LOG.info("job updateProductPriceLowMargin :  started" );
			productStockServ.updateProductPriceLowMargin(getDname(sec));
			LOG.info("job updateProductPriceLowMargin :  finshed" );
			
			builder =  Response.ok().entity("Job updateProductPriceLowMargin executed " + LocalDateTime.now());
		} catch ( Exception ex ) {
			builder = Response.status(Response.Status.BAD_REQUEST).entity(ex);			
		}
		return builder.build();
	}

	@Path("mf/search/nip/{nip}")
	@RunOnVirtualThread
	@GET
	public Response searchCustomerByNip(@PathParam("nip") String nip) {
		
		Response response = null;
		try {
			response = mfServ.searchCustomerByNip(nip);
		} catch ( Exception ex ) {
			response = Response.status(Response.Status.BAD_REQUEST)
                .entity(Json.createObjectBuilder().add("error", ex.getMessage()).add("code", Response.Status.BAD_REQUEST.getStatusCode()).build())
                .build();
		}
		return response;
	}
	
	@Path("orflow0")
	@GET
	public Response orflow0() {

		Response.ResponseBuilder builder = null;
		try {
			orderServ.executeOrflow0();
			builder =  Response.ok().entity("Job Orflow0 submited " + LocalDateTime.now());
		} catch ( Exception ex ) {
			builder = Response.status(Response.Status.BAD_REQUEST).entity(ex);			
		}
		return builder.build();
	}

	@Path("orflow1")
	@GET
	public Response orflow1() {

		Response.ResponseBuilder builder = null;
		try {
			orderServ.executeOrflow1();
			builder =  Response.ok().entity("Job Orflow1 submited " + LocalDateTime.now());
		} catch ( Exception ex ) {
			builder = Response.status(Response.Status.BAD_REQUEST).entity(ex);			
		}
		return builder.build();
	}

	@Path("orflow2")
	@GET
	public Response orflow2() {

		Response.ResponseBuilder builder = null;
		try {
			orderServ.executeOrflow2();
			builder =  Response.ok().entity("Job Orflow2 submited " + LocalDateTime.now());
		} catch ( Exception ex ) {
			builder = Response.status(Response.Status.BAD_REQUEST).entity(ex);			
		}
		return builder.build();
	}

	@Path("orflow")
	@GET
	public Response orflow() {

		Response.ResponseBuilder builder = null;
		try {
			orderServ.executeOrflow();
			builder =  Response.ok().entity("Job Orflow submited " + LocalDateTime.now());
		} catch ( Exception ex ) {
			builder = Response.status(Response.Status.BAD_REQUEST).entity(ex);			
		}
		return builder.build();
	}

	@Path("orflow-pick/{orno}")
	@GET
	public Response orflowPick(@PathParam("orno") String orno) {

		Response.ResponseBuilder builder = null;
		try {
			orderServ.executeOrflowPick(orno);
			builder =  Response.ok().entity("Job Orflow Release for pick submited " + LocalDateTime.now());
		} catch ( Exception ex ) {
			builder = Response.status(Response.Status.BAD_REQUEST).entity(ex);			
		}
		return builder.build();
	}

	private String getDname(SecurityContext sec) {
		return sec.getUserPrincipal().getName().toUpperCase().trim().replace("TDM\\", "").replace("TDM/","");
	}

	@Path("token-cdp/get")
	@GET
	public Response getTokenCdp(@Context SecurityContext sec) {
		Response.ResponseBuilder builder = null;
		try {
			var token = b2cApiConfig.requestTokenCdp(getDname(sec));
			builder =  Response.ok().entity(token);
		} catch ( Exception ex ) {
			builder = Response.status(Response.Status.BAD_REQUEST).entity(ex);			
		}
		return builder.build();
	}

	@Path("vt/get")
	@GET
	@RunOnVirtualThread
	public Response getVt(@Context SecurityContext sec) {
		Response.ResponseBuilder builder = null;
		try {
			var token = b2cApiConfig.requestTokenCdp(getDname(sec));
			builder =  Response.ok().entity("VT: " + token);
		} catch ( Exception ex ) {
			builder = Response.status(Response.Status.BAD_REQUEST).entity(ex);			
		}
		return builder.build();
	}

	@Path("foreign-orders")
	@GET
	@RunOnVirtualThread
	public Response findForeignOrders() {

		LOG.info("job ForeignOrders :  started" );

		Response.ResponseBuilder builder = null;
		try {
			orderServ.findForeignOrders();
			builder =  Response.ok().entity("Job Find Foreign Orders Executed " + LocalDateTime.now());
		} catch ( Exception ex ) {
			builder = Response.status(Response.Status.BAD_REQUEST).entity(ex);			
		}
		LOG.info("job ForeignOrders : finished" );
		return builder.build();
	}
}

package com.tadmar.mvx.b2cshopec.services.v1.mvxapi;

import MvxAPI.MvxSockJ;

public class MvxApi {
	
	private static final String API_SERVER = "S65BCC4B";
	private static final int API_PORT = 6100;
	private static final String API_CHAR_CODE = "ISO-8859-2";
	private static final String API_USER = "XJOB";
	private static final String API_PASS = "xjob99";
	
	private MvxSockJ apiObj;
	
	public MvxApi(String prgName) {
		
        this.apiObj = new MvxSockJ(API_SERVER, API_PORT, "", 0, "");
        this.apiObj.setConversionTable(API_CHAR_CODE);
        this.apiObj.DEBUG = false;
        this.apiObj.mvxInit("", API_USER, API_PASS, prgName);
	}
	
	public boolean connect() {
		
		var connectStatus = 0;
        try {
        	connectStatus = apiObj.mvxCheckConnect();
        } catch (Exception ex) {
        	connectStatus = -1;
        }
        if(connectStatus < 0 ) {
        	apiObj.mvxClose();
        }
        return connectStatus < 0 ? false : true;
	}
	
	public void close() {
		
		try {
			apiObj.mvxClose();
		} catch (Exception ex) {}	
	}

	public MvxSockJ getApiObj() {
		return apiObj;
	}
	public void setApiObj(MvxSockJ apiObj) {
		this.apiObj = apiObj;
	}

}

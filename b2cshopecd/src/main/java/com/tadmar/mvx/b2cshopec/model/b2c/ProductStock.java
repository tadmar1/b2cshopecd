package com.tadmar.mvx.b2cshopec.model.b2c;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.json.bind.annotation.JsonbNumberFormat;
import javax.json.bind.annotation.JsonbProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@EqualsAndHashCode @ToString @NoArgsConstructor @AllArgsConstructor @Builder
public class ProductStock implements Serializable {

	private static final long serialVersionUID = 1L;

	@JsonbProperty("stock_id")
	private Integer stockId;
	
	@JsonbProperty("code")
	private String code;
	
	@JsonbProperty("price")
	@JsonbNumberFormat(locale = "en_US", value = "#0.00")
	private BigDecimal price;
	
	@JsonbProperty("stock")
	private Integer stock;

	public Integer getStockId() {
		return stockId;
	}
	public void setStockId(Integer stockId) {
		this.stockId = stockId;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public BigDecimal getPrice() {
		return price;
	}
	public void setPrice(BigDecimal price) {
		this.price = price;
	}
	public Integer getStock() {
		return stock;
	}
	public void setStock(Integer stock) {
		this.stock = stock;
	}
}

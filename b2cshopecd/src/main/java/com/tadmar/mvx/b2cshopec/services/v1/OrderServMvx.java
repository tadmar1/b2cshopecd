package com.tadmar.mvx.b2cshopec.services.v1;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import com.tadmar.mvx.b2cshopec.data.Repo;
import com.tadmar.mvx.b2cshopec.model.b2c.ProductOrder;
import com.tadmar.mvx.b2cshopec.model.mvx.Mhdisl;
import com.tadmar.mvx.b2cshopec.model.mvx.Ocuscr3;
import com.tadmar.mvx.b2cshopec.model.mvx.Orflow;
import com.tadmar.mvx.b2cshopec.services.v1.mvxapi.MvxApi;
import com.tadmar.mvx.b2cshopec.services.v1.mvxapi.MvxApiException;

import static com.tadmar.mvx.b2cshopec.boot.ApplConfig.*;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.List;
import java.util.logging.Logger;

import MvxAPI.MvxSockJ;

@ApplicationScoped
public class OrderServMvx {
	
	private final static Logger LOG = Logger.getLogger(OrderServMvx.class.getName());

	public static final String TRAN_CODE_OIS100 = "OIS100MI";
	public static final String TRAN_NAME_ADD_BATCH_LINE ="AddBatchLine";
	public static final String TRAN_NAME_ADD_BATCH_HEAD ="AddBatchHead";
	public static final String TRAN_NAME_ADD_BATCH_TEXT ="AddBatchText";
	public static final String TRAN_NAME_CONFIRM ="Confirm";

	public static final String TRAN_CODE_OIS320 = "OIS320MI";
	public static final String TRAN_NAME_GET_PRICE_LINE ="GetPriceLine";
	
	public static final String TRAN_CODE_MWX410MI = "MWX410MI";
	public static final String TRAN_CODE_MHS850MI = "MHS850MI";
	
	public static final String TRAN_NAME_RELEASE_FOR_PICK ="ReleaseForPick";
	public static final String TRAN_NAME_ADD_CO_PICK ="AddCOPick";
	public static final String TRAN_NAME_CLS_CO_LINE ="ClsOrderLine";
	
	//ClsOrderLine

	@Inject Repo repo;
	
	public String createOrderHeader(int cono, Orflow orflow) throws MvxApiException {

		var objMi = getObjMi(TRAN_CODE_OIS100);
		var lastErr = "";
		boolean isDet = orflow.getOrtxcd().trim().length() == 0 ? true : false;
		
		if ( repo.checkObtype(cono, orflow.getOrcuno().trim(), "019") == false) {
			repo.createObtype(cono, orflow.getOrcuno().trim(), "019");
		}
		
		String modl = orflow.getOrspid().intValue() == SHP_SHP_TADMAR ? "WOD" : "TLD";
		if (orflow.getOrspid().intValue() == 32 || orflow.getOrspid().intValue() == 1) {
			modl = "TWC";
		}

		objMi.mvxClearFields(); 
		objMi.mvxSetField("CONO", String.valueOf(cono)); 
		objMi.mvxSetField("CUNO", orflow.getOrcuno().trim()); 
		objMi.mvxSetField("ORTP", isDet == true? "021" : "019");
		objMi.mvxSetField("ADID", orflow.getOradid()); 
		objMi.mvxSetField("FACI", "24N");
		objMi.mvxSetField("MODL", modl);
		objMi.mvxSetField("TEDL", "EXW");
		objMi.mvxSetField("CUOR", String.valueOf( orflow.getOrorsh())); 
		try {
			try {Thread.sleep(500);} catch (InterruptedException e) {}
			objMi.mvxAccess(TRAN_NAME_ADD_BATCH_HEAD);
			lastErr = objMi.mvxGetLastError();
			if (lastErr != null && lastErr.length() > 0 ) { throw new MvxApiException(lastErr); }	
		} catch (Exception ex ) { 
			throw new MvxApiException(ex.getMessage()); 
		}
		var xorno = objMi.mvxGetField("ORNO").trim();
		repo.updateBatchOrderHeaderWhlo(xorno, "11N");
		objMi.mvxClose();
		return xorno;
	}

	public void updateOrderHeaderAdd(int cono, String orno, Orflow orflow) {
	
		var pycd = "012";
		var tepy = "3";
		
		if ( orflow.getOrpaid().intValue() == SHP_PAY_ODBIOR ) {
			pycd = "011";
			tepy = "0";
		}
		if ( orflow.getOrpaid().intValue() == SHP_PAY_CASH_ON_DELIVERY) {
			pycd = "012";
			tepy = "0";
		}
		var resp = orflow.getOrchid().trim();
		repo.updateOrderHeaderAdd(MVX_COMPANY, orno, pycd, tepy, resp);
		
	}
	
	public void convertOrderLineWithSpecialPriceToDiscount(int cono, String orno, String cuno, String xorno,
			Orflow orflow) {
		
		var prrf = orflow.getOrtxcd().trim().length() == 0 ? "BR" : "NT";
		var oolinesForUpdate = repo.getOolinesForOrnoSpecialPrice(cono, orno, prrf);
		oolinesForUpdate.stream().forEach(el -> {
			repo.updateOolinesForOrnoSpecialPrice(cono, el.getOborno(), el.getObponr(), 
					el.getObsapr(), el.getObdia5(), prrf);
		});
		repo.deleteOprbasForCunoChid(cono, xorno);
		repo.deleteOpriclForCunoChid(cono, xorno);
	}

	public String createOrderLines(int cono, Orflow orflow, List<ProductOrder> products, String xorno) throws MvxApiException {
		
		var objMi = this.getObjMi(TRAN_CODE_OIS100);
		var lastErr = "";
		boolean isDet = orflow.getOrtxcd().trim().length() == 0 ? true : false;
		List<ProductOrder> lines = products;
		var msgErr = "";
		
		for ( var line : lines ) {
			try {Thread.sleep(500);} catch (InterruptedException e) {}
			var itno = repo.findItemForCode(line.getCode().trim());
			var whlo = repo.findWhloForOrderLine(MVX_COMPANY, itno);
			whlo = whlo.trim().length() == 0 ? "11N" : whlo;

			var linePrice = compPriceWithDisc(line.getPrice(), new BigDecimal(line.getDiscountPerc()));
			if (isDet) {
				linePrice = compNettoFromBrutto(linePrice, BigDecimal.ZERO);
			} else {
				linePrice = compNettoFromBrutto(linePrice, new BigDecimal(line.getTaxValue()));
			}
			var priceDef = getPrice(MVX_COMPANY, itno, orflow.getOrcuno(), line.getQuantity(), linePrice);
			if (priceDef.compareTo(linePrice) != 0) {
				createPriceSpecial(MVX_COMPANY, itno, orflow.getOrcuno().trim(), line.getQuantity(), linePrice, xorno);			
			}
			try {Thread.sleep(500);} catch (InterruptedException e) {}
			objMi.mvxClearFields();
			objMi.mvxSetField("CONO", String.valueOf(cono));
			objMi.mvxSetField("ORNO", xorno); 
			objMi.mvxSetField("WHLO", whlo);
			objMi.mvxSetField("ITNO", itno);
			objMi.mvxSetField("ORQT", String.valueOf(line.getQuantity()));
			objMi.mvxSetField("LTYP", "0");
			try {
				objMi.mvxAccess(TRAN_NAME_ADD_BATCH_LINE);
				lastErr = objMi.mvxGetLastError();
				if (lastErr != null && lastErr.length() > 0 ) { 
					throw new MvxApiException(lastErr);	
				}	
			} catch ( Exception ex ) { 
				lastErr = objMi.mvxGetLastError();
				try {
					throw new MvxApiException(ex.getMessage());
				} catch (MvxApiException mae) {
					msgErr += ";" + lastErr;
				}
			}
		}
		objMi.mvxClose();
		return msgErr;
	}

	@SuppressWarnings("unused")
	private String findWhloForOrderLine2(int cono, String itno) {
		
		
		return "";
	}
	
	public void createOrderTextApi(int cono, Orflow orflow, String xorno) throws MvxApiException {
	
		var objMi = this.getObjMi(TRAN_CODE_OIS100);
		var lastErr = "";
		objMi.mvxClearFields();
		objMi.mvxSetField("CONO", String.valueOf(cono)); 
		objMi.mvxSetField("ORNO", xorno.trim());
		objMi.mvxSetField("TYPE", "1");
		objMi.mvxSetField("TYTR", "1");
		objMi.mvxSetField("TXHR", "CO01");
		objMi.mvxSetField("PARM", orflow.getOrspidn().trim());
		
		try {
			objMi.mvxAccess(TRAN_NAME_ADD_BATCH_TEXT);
		} catch (Exception ex ) {
			lastErr = objMi.mvxGetLastError();
			try {
				throw new MvxApiException(lastErr);
			} catch (MvxApiException mae) {
				lastErr = mae.getMessage();
			}
		}
		objMi.mvxClose();
	}

	public void createOrderText(int cono, Orflow orflow, String orno) { 
		
		var text = orflow.getOrspidn().trim().toUpperCase() + " -" + String.valueOf(orflow.getOrorsh());
		var txid = repo.findOsytxhIdNext(MVX_COMPANY);
		repo.createOsytxh(MVX_COMPANY, txid, text , orno);
		repo.createOsytxl(MVX_COMPANY, txid, text);
		repo.updateOrderPrtx(MVX_COMPANY, txid, orno);

	}

	public void createOrderChrg(int cono, Orflow orflow, String orno) { 

		if ( orflow.getOrschg().compareTo(BigDecimal.ZERO) == 0 ) { return; }
		
		boolean isDet = orflow.getOrtxcd().trim().length() == 0 ? true : false;
		BigDecimal cram = orflow.getOrschg();
		if ( !isDet) {
			cram = this.compNettoFromBrutto(cram, new BigDecimal("23"));
		}
		var crd0 = orflow.getOrspidn().trim().toUpperCase();
		var crid = "";
		if ( orflow.getOrspidn().trim().toUpperCase().contains("DHL")) {
			crid = ORDER_CHRG_DHL;
		} else {
			crid = ORDER_CHRG_OTHER;
		}
		repo.createOrderChrg(MVX_COMPANY, orno, crid, crd0, cram);
	}
	
	public String confirmOrder(int cono, String xorno) throws MvxApiException {
		
		var objMi = this.getObjMi(TRAN_CODE_OIS100);
		@SuppressWarnings("unused")
		var lastErr = "";
		var orno = "";
		objMi.mvxClearFields();
		objMi.mvxSetField("CONO", String.valueOf(cono)); 
		objMi.mvxSetField("ORNO", xorno.trim());

		try {
			objMi.mvxAccess(TRAN_NAME_CONFIRM);
			orno = objMi.mvxGetField("ORNO");
		} catch (Exception ex ) {
			lastErr = objMi.mvxGetLastError();
			try {
				throw new MvxApiException(ex.getMessage());
			} catch (MvxApiException mae) {
				lastErr = mae.getMessage();
			}
		}
		objMi.mvxClose();
		return orno;
	}

	public int releaseOrderForPick(int cono, String orno) {
		
		var dlixs = repo.getDlixToReleaseForOrno(cono, orno);
		for ( var dlixi : dlixs) {

			var objMi = getObjMi(TRAN_CODE_MWX410MI);
			var lastErr = "";
			
			objMi.mvxClearFields(); 
			objMi.mvxSetField("CONO", String.valueOf(cono)); 
			objMi.mvxSetField("DLIX", String.valueOf(dlixi.getParVal()));
			objMi.mvxSetField("OLUP", "1"); 

			try {
				objMi.mvxAccess(TRAN_NAME_RELEASE_FOR_PICK);
				lastErr = objMi.mvxGetLastError();
			} catch (Exception ex ) { 
				lastErr = ex.getMessage(); 
			}
			if (lastErr.length() > 0) {
				LOG.info("Order release for pick : " + lastErr);
			}
			objMi.mvxClose();
		}
		return dlixs.isEmpty() ? 0 : dlixs.get(0).getParVal();
	}

	public int closeOrderLines(int cono, String orno) {
		
		var ornoLines = repo.getOrnoLinesForOrno(cono, orno, SHP_WHLO_DEFAULT);
		for ( var  ornoLine : ornoLines) {

			var objMi = getObjMi(TRAN_CODE_OIS100);
			var lastErr = "";
			
			objMi.mvxClearFields(); 
			objMi.mvxSetField("CONO", String.valueOf(cono)); 
			objMi.mvxSetField("ORNO", orno.trim());
			objMi.mvxSetField("PONR", String.valueOf(ornoLine.getParVal())); 

			try {
				objMi.mvxAccess(TRAN_NAME_CLS_CO_LINE);
				lastErr = objMi.mvxGetLastError();
			} catch (Exception ex ) { 
				lastErr = ex.getMessage(); 
			}
			if (lastErr.length() > 0) {
				LOG.info("Order release for pick : " + lastErr);
			}
			objMi.mvxClose();
		}
		return ornoLines.isEmpty() ? 0 : ornoLines.get(0).getParVal();
	}

	public void updateOrderForBlc(int cono,  String orno, String pyno) {
		
		var blcCode = chkCreditLimits(cono, pyno);
		if ( blcCode > 0 ) {
			repo.updateOrderForBlc(cono, orno, blcCode);
		}
	}
	public void reportOrderForPick(int cono, String orno, int dlix, Orflow orflow) {
		
		var objMi = getObjMi(TRAN_CODE_MHS850MI);
		var lastErr = "";
		
		var whlo = repo.getWhloForDlix(MVX_COMPANY, dlix);
		List<Mhdisl> disls = repo.getMhdisl2(MVX_COMPANY, orno);
		for ( var disl : disls) {
			objMi.mvxClearFields();
			objMi.mvxSetField("CONO", String.valueOf(cono));
			objMi.mvxSetField("WHLO", whlo.trim() );
			objMi.mvxSetField("CUNO", orflow.getOrcuno().trim());
			objMi.mvxSetField("E0PA", "SATTSTORE");
			objMi.mvxSetField("E065", "WMS");
			objMi.mvxSetField("RIDN", orno.trim());
			objMi.mvxSetField("RIDL", String.valueOf( disl.getUrridl()));
			objMi.mvxSetField("RIDI", String.valueOf(dlix));
			objMi.mvxSetField("PLSX", "1");
			objMi.mvxSetField("DLIX", String.valueOf(dlix));
			objMi.mvxSetField("ITNO", disl.getUritno().trim());
			objMi.mvxSetField("QTYP", ((new DecimalFormat("##0.00")).format(disl.getUrtrqt())).replace(",", "."));
			objMi.mvxSetField("WHSL", "01");
			//objMi.mvxSetField("OEND", "1");
			objMi.mvxSetField("PRFL", "*EXE");

			try {
				objMi.mvxAccess(TRAN_NAME_ADD_CO_PICK);
				lastErr = objMi.mvxGetLastError();
			} catch (Exception ex ) { 
				lastErr = ex.getMessage(); 
			}
			if (lastErr.length() > 0) {
				LOG.info("Error report order pick : " + lastErr);
			}

		}
		objMi.mvxClose();
	}
	
	private BigDecimal getPrice(int cono, String itno, String cuno, Integer b2cQty, 
			BigDecimal b2cPrice) throws MvxApiException {
		
		var objMi = this.getObjMi(TRAN_CODE_OIS320);
		var lastErr = "";
		objMi.mvxClearFields();
		objMi.mvxClearFields();
		objMi.mvxSetField("CONO", String.valueOf(cono)); 
		objMi.mvxSetField("CUNO", cuno.trim());
		objMi.mvxSetField("ITNO", itno.trim());
		objMi.mvxSetField("ORQA", String.valueOf(b2cQty)); 
		try {
			objMi.mvxAccess(TRAN_NAME_GET_PRICE_LINE);
			lastErr = objMi.mvxGetLastError();
			if (lastErr != null && lastErr.length() > 0 ) { throw new MvxApiException(lastErr); }	
		} catch (Exception ex ) { 
			lastErr = objMi.mvxGetLastError(); 
			throw new MvxApiException(ex.getMessage()); 
		}
		var mvxPriceStr = objMi.mvxGetField("LNAM").trim();
		var mvxPrice = BigDecimal.ZERO;
		try {
			mvxPrice = new BigDecimal(mvxPriceStr);
		} catch ( NumberFormatException nfe) {}
		
		objMi.mvxClose();
		return mvxPrice;
	}

	private boolean createPriceSpecial(int cono, String itno, String cuno, Integer b2bQty, 
			BigDecimal b2bPrice, String orno) {
		
		if (repo.getOprmtxForCunoCount(cono, cuno) == 0) {
			repo.createOprmtxForCuno(cono, cuno);
		}
		var unms = repo.getUnmsFromMitmas(cono, itno);
		repo.createOprbasForCunoItno(cono, cuno, itno, b2bPrice, unms, 10, orno);
		repo.createOpriclForCunoItno(cono, cuno, itno, b2bPrice, unms, 10, orno);
		
		return true;
	}

	private MvxSockJ getObjMi(String trnCode) {
		
		var apiMi = new MvxApi(trnCode);
		apiMi.connect();
		var objMi = apiMi.getApiObj();
		objMi.TRIM = true; 
		objMi.DEBUG = true;
		return objMi;
	}
	
	@SuppressWarnings("unused")
	private String convBdToApi(BigDecimal value) {
		return value != null ? String.valueOf(value).replace(",", "."): "0";
	}

	private BigDecimal compNettoFromBrutto(BigDecimal value, BigDecimal taxValue) {
		
		var ratio = (new BigDecimal("100").add( taxValue)).divide(new BigDecimal("100"),2, RoundingMode.HALF_UP);
		var newValue = value.divide(ratio,2, RoundingMode.HALF_UP);
		return newValue;
	}

	private BigDecimal compPriceWithDisc(BigDecimal value, BigDecimal discValue) {
		
		var newValue = (new BigDecimal(100).subtract(discValue)).multiply(value)
				.divide(new BigDecimal("100"),2, RoundingMode.HALF_UP);	
		return newValue;
	}
	
	public Ocuscr3 resetOcuscr(int cono, String pyno) {
		
		var ocuscr = repo.getOcuscrForCuno(cono, pyno);
		repo.resetOcuscrForCuno(cono, pyno);
		return ocuscr;
	}

	public void recreateOcuscr(int cono, String pyno, Ocuscr3 ocuscr, String orno) {
		repo.recreateOcuscrForCuno(cono, pyno, ocuscr);
	}

    private int chkCreditLimits(int cono, String cuno) {
    	
    	var o = repo.getOcuscrForCuno(cono, cuno);
    	var blCode = 0;
    	if (o.getOkcrlm().compareTo(o.getOktdin()) < 0) { blCode = 1; } 
    	else if (o.getOkcrl2().compareTo(o.getOktoin()) < 0 ) { blCode = 2; }   		
    	else if (o.getOkcrl3().compareTo(o.getOktoin().add(o.getOktblg())) < 0 ) { blCode = 3; }
    	else if (o.getOkodud().intValue() < o.getOkodue().intValue() ) { blCode = 4; }   		
    	return blCode;
    }

}


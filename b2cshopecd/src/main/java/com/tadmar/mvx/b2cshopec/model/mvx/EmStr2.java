package com.tadmar.mvx.b2cshopec.model.mvx;

import javax.persistence.Entity;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Entity
@EqualsAndHashCode @ToString @NoArgsConstructor @AllArgsConstructor @Builder

public class EmStr2 {

    @Id
    private String parVal1;
    private String parVal2;
    
	public String getParVal1() {
		return parVal1;
	}
	public void setParVal1(String parVal1) {
		this.parVal1 = parVal1;
	}
	public String getParVal2() {
		return parVal2;
	}
	public void setParVal2(String parVal2) {
		this.parVal2 = parVal2;
	}

}

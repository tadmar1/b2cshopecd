package com.tadmar.mvx.b2cshopec.services.v1;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;

import java.util.logging.Logger;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.json.bind.JsonbBuilder;
import javax.ws.rs.WebApplicationException;

import org.eclipse.microprofile.rest.client.inject.RestClient;

import com.tadmar.mvx.b2cshopec.boot.ApplConfig;
import com.tadmar.mvx.b2cshopec.data.Dicts;
import com.tadmar.mvx.b2cshopec.data.Repo;
import com.tadmar.mvx.b2cshopec.model.b2c.Address;
import com.tadmar.mvx.b2cshopec.model.b2c.OrderF;
import com.tadmar.mvx.b2cshopec.model.b2c.ParcelF;
import com.tadmar.mvx.b2cshopec.model.mvx.Ocuscr3;
import com.tadmar.mvx.b2cshopec.model.mvx.Orflow;
import com.tadmar.mvx.b2cshopec.rest.client.ApiExtV1Client;
import com.tadmar.mvx.b2cshopec.services.v1.mvxapi.MvxApiException;
import com.tadmar.mvx.b2cshopec.util.DateUtils;
import com.tadmar.mvx.b2cshopec.util.RestClientUtil;
import com.tadmar.mvx.b2cshopec.model.mvx.EmStr;


import io.quarkus.mailer.Mail;
import io.quarkus.mailer.Mailer;

import static com.tadmar.mvx.b2cshopec.boot.ApplConfig.*;

@ApplicationScoped
public class OrderServ {

	private final static Logger LOG = Logger.getLogger(OrderServ.class.getName());
	
	@Inject ApplConfig ac;
	@Inject Repo repo;
	@Inject Dicts dicts;
	@Inject OrderServMvx orderServMvx;
	@Inject OrderServShp orderServShp;
	@Inject CdpServ cdpServ;
	@Inject CustomerServMvx customerServMvx;
	@Inject Mailer mailer;
	@Inject @RestClient ApiExtV1Client apiExtV1Client;

	
	@PostConstruct
	void init() {}

	public void executeOrflow0() {
		
		try (var exec = Executors.newVirtualThreadPerTaskExecutor()) {
			exec.execute( () -> {
				LOG.info("job Orflow0 :  started" );
				orflowRegister();
				orflowChkStatuses();
				LOG.info("job Orflow0 :  finished" );
			});
		}
	}

	public void executeOrflow() {
		
		try (var exec = Executors.newVirtualThreadPerTaskExecutor()) {
			exec.execute( () -> {
				LOG.info("job Orflow :  started" );
				orflowCreateOrderMvx();
				LOG.info("job Orflow :  finished" );
			});		
		}
	}	
	
	public void executeOrflow1() {
		
		try (var exec = Executors.newVirtualThreadPerTaskExecutor()) {
			exec.execute( () -> {
				LOG.info("job Orflow1 :  started" );
				orflowChkDlnu();
				orflowChkShpDlnu();
				LOG.info("job Orflow1 :  finished" );
			});		
		}
	}
	
	public void executeOrflow2() {
		
		try (var exec = Executors.newVirtualThreadPerTaskExecutor()) {
				exec.execute( () -> {
				LOG.info("job Orflow2 :  started" );
				orflowChkInvNotSend();
				LOG.info("job Orflow2 :  finished" );
			});		
		}
	}
	
	public void executeOrflowPick(String orno) {
		
		try (var exec = Executors.newVirtualThreadPerTaskExecutor()) {
			exec.execute( () -> {
				LOG.info("job OrflowPick :  started" );
				orflowReleaseForPickOrderMvx(orno);
				LOG.info("job OrflowPick :  finished" );
			});		
		}
	}
	
	public void orflowReleaseForPickOrderMvx(String orno) {
		orderServMvx.releaseOrderForPick(MVX_COMPANY, orno);
	}

	public void findForeignOrders() {
		
		var fOrders = repo.findForeignOrders(MVX_COMPANY);
		if (!fOrders.isEmpty()) {
			fOrders.stream().forEach(o -> { 
				var orno = o.getParVal().split("-")[0];	
				orderServMvx.closeOrderLines(MVX_COMPANY, orno); 
			});

			String orderIds = fOrders.stream().map( EmStr::getParVal).collect(Collectors.joining(", "));
			var subject = "B2C Informacja o ZS-ach obcych hurtowni z magazynu sklepu";
			var body = repo.getMailTemplate("FindForeignOrders").replace("@Ids", orderIds);
			var mail = Mail.withHtml(BOK_MAIL, subject, body);
			mail.addCc(BOK_CDP_MAIL);
			mail.addBcc(ADMIN_MAIL);
			try {
				mailer.send(mail);
			} catch (Exception ex) {
				LOG.info(ex.getMessage());	
			}
		} 
		
	}		
	
	private void orflowChkInvNotSend() {
		
		var orflows = repo.getOrdersInvNotSend();
		if (!orflows.isEmpty()) {
			String orflowsIds = orflows.stream().map(EmStr::getParVal).collect(Collectors.joining(", "));
			var subject = "B2C Informacja o niewysłanych fakturach ";
			var body = repo.getMailTemplate("ChkOrdersInvNotSend").replace("@Ids", orflowsIds);
			var mail = Mail.withHtml(BOK_MAIL, subject, body);
			mail.addCc(MGR_MAIL);
			mail.addBcc(ADMIN_MAIL);
			try {
				mailer.send(mail);
			} catch (Exception ex) {
				LOG.info(ex.getMessage());	
			}
		}
	}
	
	private void orflowChkDlnu() {
		
		var dtFr = LocalDateTime.now().minusDays(10).format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
		var dtTo = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
		var docDlnus = cdpServ.getDlnu("", dtFr, dtTo);
		var dtFrSh = LocalDateTime.now().minusDays(10).format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
		var docDlnusSh = cdpServ.getDlnuSh("", dtFrSh, dtTo);
		LOG.info("docDlnusSh:" + docDlnusSh.size());
		var orflows = repo.getOrdersWithDlnoEmpty();
		orflows.stream().forEach(of -> {
			docDlnus.stream().filter(dl -> dl.getExternalId().trim().equals(of.getOrorno().trim()) ).forEach(ou -> {
				if (ou.getShippingNumber().trim().length() > 0 ) {
					if (ou.getShippingNumber().trim().length() > 50 ) {
						ou.setShippingNumber(ou.getShippingNumber().substring(0, 49));
					}
					of.setOrdlnu(ou.getShippingNumber());
					repo.updateRec(of);
					LOG.info("ORDLNU UPDATE1: " + of.getOrorno());		

				} else {
					docDlnusSh.stream().filter( dlsh-> dlsh.getParVal1() != null && ou.getExternalId() != null 
							&& dlsh.getParVal1().trim().equals(ou.getExternalId().trim()) && !dlsh.getParVal2().startsWith("TW") ).forEach( dlshf -> {
						of.setOrdlnu(dlshf.getParVal2().trim());
						repo.updateRec(of);
						LOG.info("ORDLNU UPDATE2: " + of.getOrorno());		
					});		
				}
			});
		});
	}

	private void orflowChkShpDlnu() {
		
		var orflows = repo.getOrdersLastWithDlno();
		var orShMinId = orflows.get(orflows.size() -1 ).getOrorsh();
		List<ParcelF> parcels = orderServShp.getParcelsF(orShMinId, "");
		
		parcels.stream().filter(p -> p.getShippingCode() == null || p.getShippingCode().trim().length() == 0 ).forEach(pn -> {
			orflows.stream().filter(o -> o.getOrorsh().intValue() == pn.getOrderId().intValue()).forEach( or -> {
				var ordlnu = or.getOrdlnu().trim();
				LOG.info("TO UPDATE PARCEL code" + ordlnu + ": "+ pn.getParcelId());
				pn.setShippingCode(ordlnu);
				try {
				apiExtV1Client.updateParcel(ac.getAuthKey(), pn.getParcelId(), pn);
				} catch ( WebApplicationException wae) {
					LOG.info("ERROR UPDATE PARCEL code "+ ordlnu + ": "+ pn.getParcelId());
				}
			});
		});
		
	}

	private void orflowRegister() {
	
		List<OrderF> ordersF = orderServShp.getOrdersF(1,6,"");
		List<Orflow> orflows = repo.getOrdersRegistered();
		
		
		ordersF.stream().filter(o -> 
			o.getStatusId().intValue() == SHP_ORD_COMPLETED || o.getStatusId().intValue() == SHP_ORD_REGISTERED
		)
		.forEach(on -> {
			if (orflows.stream().filter(of -> of.getOrorsh().intValue() == on.getOrderId().intValue()).count() == 0 ) {
				LOG.info("Imported New OrderF: " + on.toString());
				var orflow = convertOrderFToOrflow(on);
				orflow = repo.createRec(orflow);
				var cunoAdid = customerServMvx.findOrCreateCustomerMvx(orflow);
				if ( !cunoAdid.getCuno().contains("!") && !cunoAdid.getAdid().contains("!")) {
					orflow.setOrcuno(cunoAdid.getCuno().trim());
					orflow.setOradid(cunoAdid.getAdid().trim());
					orflow.setOrorst(1);
					orflow = repo.updateRec(orflow);
				}
				try {	Thread.sleep(1000); } catch (InterruptedException e) {}
			} else {
				//LOG.info(on.toString());
			}
		});
		
		ordersF.stream().filter(o -> o.getStatusId().intValue() == SHP_ORD_COMPLETED).forEach(on -> {
			orflows.stream().filter(of -> of.getOrorsh().intValue() == on.getOrderId().intValue() && of.getOrstsh().intValue() == 1).forEach(ofu -> {
				ofu.setOrcnot(ofu.getOrcnot().replace("$$$$ ",""));
				ofu.setOrstsh(6);
				if(ofu.getOrorst().intValue() == -1000 && ofu.getOrorno() != null && ofu.getOrorno().trim().length() > 0 ) {
					ofu.setOrorst(2);
				}	
				if(ofu.getOrorst().intValue() == -1000 && (ofu.getOrorno() == null || ofu.getOrorno().trim().length() == 0 )) {
					ofu.setOrorst(0);
				}	
				
				LOG.info("Updated OrderF: " + on.toString());
				repo.updateRec(ofu);
			});	
		});
	}

	private void orflowChkStatuses() {
		repo.orflowChkStatuses(MVX_COMPANY).stream().forEach( el -> {
			repo.orflowUpdateStatuses(el.getParVal1(), Integer.valueOf(el.getParVal2()));


		});
	}
	
	private void orflowCreateOrderMvx() {
		
		List<Orflow> orflows = repo.getOrdersForStatuses(MVX_COMPANY, MVX_ORD_REGISTERED_WITH_SHIP_ADDR, MVX_ORD_REGISTERED_WITH_SHIP_ADDR);
		
		for ( var orflow : orflows) {
			if (!orflow.getOradid().trim().equals("!") && !orflow.getOrcuno().trim().equals("!") ){
				if ( 
					orflow.getOrstsh().intValue() == SHP_ORD_COMPLETED  || orflow.getOrstsh().intValue() == SHP_ORD_REGISTERED
				) {
					try {
						var xorno = orderServMvx.createOrderHeader(MVX_COMPANY, orflow);
						var products = orderServShp.getOrderProducts(orflow.getOrorsh(), orflow.getOrorsh(), "");
						orderServMvx.createOrderLines(MVX_COMPANY, orflow, products, xorno);
						var orno = "";
						var pyno = repo.getPynoForCuno(MVX_COMPANY, orflow.getOrcuno().trim());
						Ocuscr3 ocuscr = orderServMvx.resetOcuscr(MVX_COMPANY, pyno);
						try {
							RestClientUtil.delay(1000);
							orno = orderServMvx.confirmOrder(MVX_COMPANY, xorno);
							RestClientUtil.delay(1000);
						} catch (Exception ex) {LOG.info(ex.getMessage());
						} finally {
							orderServMvx.recreateOcuscr(MVX_COMPANY, pyno, ocuscr, orno);
						}
						orderServMvx.updateOrderForBlc(MVX_COMPANY, orno, pyno);
						orderServMvx.createOrderText(MVX_COMPANY, orflow, orno);
						orderServMvx.createOrderChrg(MVX_COMPANY, orflow, orno);
						orderServMvx.updateOrderHeaderAdd(MVX_COMPANY, orno, orflow);
						repo.updateOrderHeaderOrtp(MVX_COMPANY, orno);
						orderServMvx.convertOrderLineWithSpecialPriceToDiscount(MVX_COMPANY, orno, "", xorno, orflow);
						orflow.setOrornx(xorno);
						orflow.setOrorno(orno);
						orflow.setOrorst(2);
						repo.updateRec(orflow);
					
					
					} catch (MvxApiException e) {
						LOG.info("Create order error for id " + orflow.getOrunid() + " " + e.getMessage());
					} finally {
						repo.setWhlInt(MVX_COMPANY);
					}
				}	
			}
		}
	}

	private Orflow convertOrderFToOrflow(OrderF orderF) {

		var pkpo =  findPickupPoint(orderF);
		var cdam = orderF.getPaymentId().intValue() == SHP_PAY_CASH_ON_DELIVERY || ( orderF.getPaymentId().intValue() == SHP_ALLEGRO  && orderF.getShippingId().intValue() == 27 ) ? 
			orderF.getSum() : BigDecimal.ZERO ;
		
		int pdfInv = orderF.getPaymentId().intValue() != SHP_ALLEGRO ? 1 : 0;

		//for (var af : orderF.getAdditionalFields()) {
			//if ( af.getFieldId().trim().equals("20") && af.getValue().trim().equals("1") ) {
			//	pdfInv = 1;
			//}
		//}

		var ororst = orderF.getStatusId().intValue() == SHP_ORD_COMPLETED ? 0 : -1000;
		var orcnot = orderF.getStatusId().intValue() == SHP_ORD_COMPLETED ? orderF.getNotes().trim() : "$$$$ " + orderF.getNotes().trim();
		
		if (orderF.getShippingId().intValue() == 30) {
			orderF.setShippingId(25);
		}
		
		var orflow = Orflow.builder()
			.ororno("")
			.ororst(ororst)			//.ororst(adid.equals(EMPTY_ADID)? MVX_ORD_REGISTERED : MVX_ORD_REGISTERED_WITH_SHIP_ADDR )
			.orornx("")
			.ororsh(orderF.getOrderId())
			.orstsh(orderF.getStatusId())
			.orpaid(orderF.getPaymentId())
			.orpaidn(dicts.getPaymentNameById(orderF.getPaymentId()))
			.orspid(orderF.getShippingId())
			.orspidn(dicts.getShippingNameById(orderF.getShippingId()))
			.orcuno("!")
			.oradid("!")
			.orssum(orderF.getSum())
			.orschg(orderF.getShippingCost())
			.orrgdt(DateUtils.NowToDateMvxI())
			.orrgtm(DateUtils.NowToTimeMvxI())
			.orlmdt(DateUtils.NowToDateMvxI())
			.orlmtm(DateUtils.NowToTimeMvxI())
			.orpaad(JsonbBuilder.create().toJson(addressEntityToList(orderF.getBillingAddress())))
			.orspad(JsonbBuilder.create().toJson(addressEntityToList(orderF.getDeliveryAddress())))
			.ortxcd(orderF.getBillingAddress().getTaxIdentificationNumber().trim())
			.orpycd("")
			.ortepy("")
			.orpaen(0)
			.orcuma(orderF.getEmail().trim())
			.orchid("XJOB")
			
			.orpkpo(pkpo)
			.orcdam(cdam)
			.orisam(orderF.getSum())
			.orivno("")
			.ordlnu("")
			.orpkst("DEL_SEND")
			.orbaph(phoneNumberFormat(orderF.getBillingAddress().getPhone().trim(), orderF.getPaymentId()))
			.ordaph(phoneNumberFormat(orderF.getDeliveryAddress().getPhone().trim(), orderF.getPaymentId()))
			.orcnot(orcnot)
			.orpdfi(pdfInv)
			.build();
		
		return orflow;
	}

	private String phoneNumberFormat(String srcNumber, int paid) {

		var dstNumber = srcNumber;
		if (paid == SHP_ALLEGRO) {
			if (dstNumber.trim().length() > 9 ) {
				dstNumber = dstNumber.substring(dstNumber.trim().length() - 9);
			}
		}
		return dstNumber;
	}

	private String findPickupPoint(OrderF orderF) {
		var pp = "";
		
		if (orderF.getPickupPoint() != null) {
			pp = orderF.getPickupPoint().trim();
		} else {
			var addFds = orderF.getAdditionalFields();
			if (addFds.stream().filter(af -> af.getFieldId().trim().equals("15") && af.getValue().trim().startsWith("DHL POP")).count() > 0 ) {
				pp = addFds.stream().filter(af -> af.getFieldId().trim().equals("16")).collect(Collectors.toList()).get(0).getValue();
			}
		}
		
		return pp;
	}
	
	
	private List<Address> addressEntityToList(Address address) {
		
		List<Address> addL = new ArrayList<>();
		addL.add(address);
		return addL;
	}

}

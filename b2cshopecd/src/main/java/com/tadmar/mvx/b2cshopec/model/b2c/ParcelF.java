package com.tadmar.mvx.b2cshopec.model.b2c;

import java.io.Serializable;

import javax.json.bind.annotation.JsonbProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

@NoArgsConstructor @AllArgsConstructor @Builder
public class ParcelF implements Serializable {

	private static final long serialVersionUID = 1L;

	@JsonbProperty("parcel_id")
	private Integer parcelId;

	@JsonbProperty("order_id")
	private Integer orderId;
	
	@JsonbProperty("shipping_id")
	private Integer shippingId;
	
	@JsonbProperty("shipping_code")
	private String shippingCode;

	public Integer getParcelId() {
		return parcelId;
	}

	public void setParcelId(Integer parcelId) {
		this.parcelId = parcelId;
	}

	public Integer getOrderId() {
		return orderId;
	}

	public void setOrderId(Integer orderId) {
		this.orderId = orderId;
	}

	public Integer getShippingId() {
		return shippingId;
	}

	public void setShippingId(Integer shippingId) {
		this.shippingId = shippingId;
	}

	public String getShippingCode() {
		return shippingCode;
	}

	public void setShippingCode(String shippingCode) {
		this.shippingCode = shippingCode;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((orderId == null) ? 0 : orderId.hashCode());
		result = prime * result + ((parcelId == null) ? 0 : parcelId.hashCode());
		result = prime * result + ((shippingCode == null) ? 0 : shippingCode.hashCode());
		result = prime * result + ((shippingId == null) ? 0 : shippingId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ParcelF other = (ParcelF) obj;
		if (orderId == null) {
			if (other.orderId != null)
				return false;
		} else if (!orderId.equals(other.orderId))
			return false;
		if (parcelId == null) {
			if (other.parcelId != null)
				return false;
		} else if (!parcelId.equals(other.parcelId))
			return false;
		if (shippingCode == null) {
			if (other.shippingCode != null)
				return false;
		} else if (!shippingCode.equals(other.shippingCode))
			return false;
		if (shippingId == null) {
			if (other.shippingId != null)
				return false;
		} else if (!shippingId.equals(other.shippingId))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "ParcelF [parcelId=" + parcelId + ", orderId=" + orderId + ", shippingId=" + shippingId
				+ ", shippingCode=" + shippingCode + "]";
	}
}

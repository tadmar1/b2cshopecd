package com.tadmar.mvx.b2cshopec.model.b2c;

import java.io.Serializable;
import java.util.List;

import javax.json.bind.annotation.JsonbProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@EqualsAndHashCode @ToString @NoArgsConstructor @AllArgsConstructor @Builder
public class ProductStockList implements Serializable {

	private static final long serialVersionUID = 1L;

	@JsonbProperty("count")
	private Integer count;
	
	@JsonbProperty("pages")
	private Integer pages;

	@JsonbProperty("page")
	private Integer page;

	@JsonbProperty("list")
	private List<ProductStock> list;

	public Integer getCount() {
		return count;
	}
	public void setCount(Integer count) {
		this.count = count;
	}
	public Integer getPages() {
		return pages;
	}
	public void setPages(Integer pages) {
		this.pages = pages;
	}
	public Integer getPage() {
		return page;
	}
	public void setPage(Integer page) {
		this.page = page;
	}
	public List<ProductStock> getList() {
		return list;
	}
	public void setList(List<ProductStock> list) {
		this.list = list;
	}
}

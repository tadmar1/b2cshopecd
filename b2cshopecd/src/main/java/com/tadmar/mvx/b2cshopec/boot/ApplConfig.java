package com.tadmar.mvx.b2cshopec.boot;

import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.eclipse.microprofile.rest.client.inject.RestClient;

import com.tadmar.mvx.b2cshopec.rest.client.ApiExtV1Client;

import io.quarkus.runtime.Startup;

@Startup
@ApplicationScoped
public class ApplConfig {

	private static final Logger LOG = Logger.getLogger(ApplConfig.class.getName());

	@ConfigProperty(name = "com.tadmar.mvx.b2cshopec.API_B2C_TOKEN_TMP") public String API_B2C_TOKEN_TMP;
	//@ConfigProperty(name = "com.tadmar.mvx.b2cshopec.MVX_COMPANY") public int MVX_COMPANY;

	public static final int MVX_COMPANY = 200;
	
	public static final int SHP_ORD_REGISTERED = 1;
	public static final int SHP_ORD_ON_DELIVERY = 5;
	public static final int SHP_ORD_COMPLETED = 6;

	public static final int MVX_ORD_REGISTERED = 0;
	public static final int MVX_ORD_REGISTERED_WITH_SHIP_ADDR = 1;
	
	public static final int SHP_SHP_TADMAR = 1;
	public static final int SHP_PAY_ODBIOR = 1;
	public static final int SHP_PAY_CASH_ON_DELIVERY = 20; //2
	public static final int SHP_ALLEGRO = 13; 

	public static final int PRODUCT_PRICE_STATUS_TO_CHNAGE = 15;
	public static final int PRODUCT_PRICE_STATUS_CHNAGED = 20;
	public static final int SHP_API_PAGE_SIZE = 50;
	
	public static final String ORDER_CHRG_OTHER = "OPTRAN";
	public static final String ORDER_CHRG_DHL = "TRANS";
	
	public static final String EMPTY_CUNO = "!";
	public static final String EMPTY_CUNO_ERROR = "!ERROR";
	public static final String EMPTY_ADID ="!";
	public static final String EMPTY_ADID_ERROR ="!ERROR";
	
	public static final String ADMIN_MAIL = "roman.jasinski@tadmar.pl";
	public static final String MGR_MAIL = "agata.bialy@tadmar.pl";
	public static final String BOK_MAIL = "sklep@tadmar.pl";
	public static final String BOK_CDP_MAIL = "bok_cd_poznan@tadmar.pl";
	
	public static final String SHP_WHLO_DEFAULT = "11O";
	
	@Inject B2cApiConfig b2cApiConfig;
	private String accessToken;
	private String tokenType;
	private String authKey;
	
	@Inject @RestClient ApiExtV1Client apiExtV1Client;
	
	@PostConstruct
	void init() {
		
		try (var exec = Executors.newSingleThreadScheduledExecutor()) {
			exec.schedule( ()-> {
				initApiB2cToken();
				//outls = initOutls();			
				}, 30L, TimeUnit.SECONDS
			);
		}
		
	}

	private void initApiB2cToken() {
		
		LOG.info("process GetToken started ");
		accessToken = b2cApiConfig.requestToken();
		tokenType = b2cApiConfig.getTokenType();
		authKey = tokenType + " " + accessToken;
		LOG.info(this.accessToken);
		LOG.info("process GetToken finished ");
	}

	
	public String getAccessToken() {
		return accessToken;
	}
	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}
	public String getTokenType() {
		return tokenType;
	}
	public void setTokenType(String tokenType) {
		this.tokenType = tokenType;
	}
	public String getAuthKey() {
		return authKey;
	}
	public void setAuthKey(String authKey) {
		this.authKey = authKey;
	}
}

package com.tadmar.mvx.b2cshopec.model.mvx;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table (name="SHBAL", schema="B2CSHP")
@Getter @Setter @EqualsAndHashCode @ToString @NoArgsConstructor @AllArgsConstructor @Builder
public class Shbal implements Serializable {
	
	private static final long serialVersionUID = 1L;
 
	@Id @GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer prunid;
	private Integer prodId;
	private Integer stockId;
	private BigDecimal price;
	private Integer prctyp;	
	private Integer extend;
	private Integer active;
	private Integer defaul;
	private BigDecimal stock;
	private Integer warlev;	
	private BigDecimal sold;	
	private String code;	
	private String prean;
	private BigDecimal weight;	
	private Integer weityp;	
	private Integer avalId;
	private BigDecimal packag;		
	private BigDecimal prcwho;	
	private BigDecimal prcspe;	
	private Integer prcwht;	
	private Integer prcspt;	
	private BigDecimal prcbuy;	
	private Integer calcui;
	private BigDecimal calcur;
	private String prname;
	private String desshr;
	private String desful;
	private String attrib;
	private String attris;
	private String dtAd;
	private String dtEd;
	private Integer gaugId;
}	

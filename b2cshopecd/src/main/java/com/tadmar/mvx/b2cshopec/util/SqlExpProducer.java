package com.tadmar.mvx.b2cshopec.util;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Produces;
import javax.enterprise.inject.spi.InjectionPoint;

import com.tadmar.mvx.b2cshopec.util.SqlExpAnotations.SqlExpKey;


@ApplicationScoped
public class SqlExpProducer {

	@Produces @SqlExpKey(key="")
	public SqlExp produceSqlExp(InjectionPoint injectionPoint){
		
		String key = injectionPoint.getAnnotated().getAnnotation(SqlExpKey.class).key();
		return new SqlExp(key);
	}
}

package com.tadmar.mvx.b2cshopec.model.b2c;

import java.io.Serializable;

import javax.json.bind.annotation.JsonbProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@EqualsAndHashCode @ToString @NoArgsConstructor @AllArgsConstructor @Builder
public class Address implements Serializable {

	static final long serialVersionUID = 1L;

	@JsonbProperty("address_id")
	private Integer addressId;
	
	@JsonbProperty("order_id")
	private Integer orderId;
	
	@JsonbProperty("type")
	private Integer type;
	
	@JsonbProperty("firstname")
	private String firstname;
	
	@JsonbProperty("lastname")
	private String lastname;

	@JsonbProperty("company")
	private String company;

	@JsonbProperty("pesel")
	private String pesel;
	
	@JsonbProperty("city")
	private String city;

	@JsonbProperty("postcode")
	private String postcode;

	@JsonbProperty("street1")
	private String street1;

	@JsonbProperty("street2")
	private String street2;

	@JsonbProperty("state")
	private String state;

	@JsonbProperty("country")
	private String country;

	@JsonbProperty("phone")
	private String phone;

	@JsonbProperty("country_code")
	private String countryCode;
	
	@JsonbProperty("tax_identification_number")
	private String taxIdentificationNumber;

	public Integer getAddressId() {
		return addressId;
	}
	public void setAddressId(Integer addressId) {
		this.addressId = addressId;
	}
	public Integer getOrderId() {
		return orderId;
	}
	public void setOrderId(Integer orderId) {
		this.orderId = orderId;
	}
	public Integer getType() {
		return type;
	}
	public void setType(Integer type) {
		this.type = type;
	}
	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public String getCompany() {
		return company;
	}
	public void setCompany(String company) {
		this.company = company;
	}
	public String getPesel() {
		return pesel;
	}
	public void setPesel(String pesel) {
		this.pesel = pesel;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getPostcode() {
		return postcode;
	}
	public void setPostcode(String postcode) {
		this.postcode = postcode;
	}
	public String getStreet1() {
		return street1;
	}
	public void setStreet1(String street1) {
		this.street1 = street1;
	}
	public String getStreet2() {
		return street2;
	}
	public void setStreet2(String street2) {
		this.street2 = street2;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getCountryCode() {
		return countryCode;
	}
	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}
	public String getTaxIdentificationNumber() {
		return taxIdentificationNumber;
	}
	public void setTaxIdentificationNumber(String taxIdentificationNumber) {
		this.taxIdentificationNumber = taxIdentificationNumber;
	}
}

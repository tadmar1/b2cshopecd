package com.tadmar.mvx.b2cshopec.util;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.enterprise.util.Nonbinding;
import javax.inject.Qualifier;

public class SqlExpAnotations {
	@Target({ElementType.FIELD, ElementType.METHOD})
	@Retention(RetentionPolicy.RUNTIME)
	@Qualifier
	public @interface SqlExpKey {
	    @Nonbinding String key();
	}
}

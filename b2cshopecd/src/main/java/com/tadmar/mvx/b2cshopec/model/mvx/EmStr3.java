package com.tadmar.mvx.b2cshopec.model.mvx;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Getter @Setter @EqualsAndHashCode @ToString @NoArgsConstructor @AllArgsConstructor @Builder
public class EmStr3 implements Serializable {
	
	private static final long serialVersionUID = 1L;
 
	@Id
	private String parVal1;
	private String parVal2;
	private String parVal3;
}	

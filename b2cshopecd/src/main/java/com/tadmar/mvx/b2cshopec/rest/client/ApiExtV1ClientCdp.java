package com.tadmar.mvx.b2cshopec.rest.client;

import javax.ws.rs.Consumes;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;

@RegisterRestClient(configKey="cdp-api")
@Path("/")
@Produces("application/json")
@Consumes("application/json")
public interface ApiExtV1ClientCdp {
	
	@Path("GetDocuments")
	@POST
	Response getDocDlnos(@HeaderParam("Authorization") String authKey, 
			String jsonParams); 

	@Path("GetShippingCarriers")
	@POST
	Response getDocDlnosSh(@HeaderParam("Authorization") String authKey, 
			String jsonParams); 
}

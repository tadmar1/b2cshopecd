package com.tadmar.mvx.b2cshopec.rest;

import javax.json.bind.JsonbBuilder;

public class ApiUtils {
	
	public static <T> String postCreateBody(String ent, T rec) {
		
		var json = "{\"" + ent + "\":" + JsonbBuilder.create().toJson(rec) + // "}" ;
		",\"saveOptions\": true}";
		return json;
	}

	public static <T> String putCreateBody(String ent, T rec) {
		
		var json = "{\"" + ent + "\":" + JsonbBuilder.create().toJson(rec) + // "}" ;
		",\"saveOptions\": true}";
		return json;
	}

}

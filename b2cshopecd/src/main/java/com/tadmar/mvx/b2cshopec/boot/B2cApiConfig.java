package com.tadmar.mvx.b2cshopec.boot;

import java.util.Base64;
//import java.util.Base64;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;

import org.eclipse.microprofile.config.inject.ConfigProperty;

import com.jayway.jsonpath.JsonPath;
import com.tadmar.mvx.b2cshopec.model.b2c.Token;

@ApplicationScoped
public class B2cApiConfig {

	private static final Logger LOG = Logger.getLogger(B2cApiConfig.class.getName());
	
	@ConfigProperty(name = "com.tadmar.mvx.b2cshopec.API_B2C_URL") public String API_B2C_URL;
	@ConfigProperty(name = "com.tadmar.mvx.b2cshopec.API_B2C_USER") public String API_B2C_USER;
	@ConfigProperty(name = "com.tadmar.mvx.b2cshopec.API_B2C_PASS") public String API_B2C_PASS;
	@ConfigProperty(name = "com.tadmar.mvx.b2cshopec.API_B2C_URL_TOKEN") public String API_B2C_URL_TOKEN;

	@ConfigProperty(name = "com.tadmar.mvx.b2cshopec.API_CDP_URL_TOKEN") public String API_CDP_URL_TOKEN;

	@PostConstruct
	void init() {}
	
	public String requestToken() {
		
		var accessToken = "";
		
		try {
			var client = ClientBuilder.newClient();
			WebTarget target = client.target(API_B2C_URL_TOKEN);
			var authHeader = "Basic " + Base64.getUrlEncoder().encodeToString((API_B2C_USER.trim() + ":" + API_B2C_PASS.trim()).getBytes());
			var response = target.request(MediaType.TEXT_PLAIN_TYPE).header("Authorization", authHeader).post(null);
	
			Token respBody = response.readEntity(Token.class);
			response.close();
			client.close();
			accessToken = respBody.getAccessToken();
		} catch (Exception ex) {
			LOG.info("Can not retrive token from  " + API_B2C_URL_TOKEN);
		}
		
		return accessToken;
	}
	
	public String getTokenType() {
		return "Bearer";
	}

	public String requestTokenCdp(String user) {
		
		var accessToken = "";
		try {
			var client = ClientBuilder.newClient();
			WebTarget target = client.target(API_CDP_URL_TOKEN);
			var response = target.request(MediaType.APPLICATION_JSON).get();
	
			var respBody = response.readEntity(String.class);
			response.close();
			client.close();
			String jsonExp = "$.token";
			accessToken = JsonPath.parse(respBody).read(jsonExp);
		} catch (Exception ex) {
			LOG.info("Can not retrive token from  " + API_CDP_URL_TOKEN);
		}
		
		return getTokenTypeCdp() + " " + accessToken.trim();
	}	

	public String getTokenTypeCdp() {
		return "Bearer";
	}

}
